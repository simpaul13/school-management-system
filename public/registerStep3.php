<!DOCTYPE html>
<html lang="en">
<head>
	<title>ONLINE REGISTRATION</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <link rel="stylesheet" type="text/css" href="registerHome.css">

</head>
<body>

<nav class="navbar" style="background-color: #006600; color: white;">
  <span class="navbar-brand mb-0 h1">ONLINE REGISTRATION</span>
</nav>

<!---CONTENT-->
<!---second container-->
<div class="container">
  <div class="row" style="height: 600px;">

    <div class="col-sm-12" style="margin-top: 50px; background-color: whitesmoke; padding: 30px;">
      <h5>TRACKING NUMBER:1</h5>

      
      <p><i>Take a picture of your tracking number for proof of registration.</i></p>
      <br>
      <br>
      <br>
      <h5>REQUIREMENTS:</h5>
      <br>
      <p><span style="color: red">*</span> Submit the <b>Registration form</b> with:</p>
      <p style="margin-left: 20px;"><span style="color: red">*</span> Birth Certificate NSA Copy</p>
      <p style="margin-left: 20px;"><span style="color: red">*</span> Baptismal Certificate</p>
      <p style="margin-left: 20px;"><span style="color: red">*</span> Previous form 138 (if transferee)</p>
      <br>
      <h5 style="font-size: 15px;"><span style="color: red;">*</span> THE PAYMENT WILL BE IN THE SCHOOL</h5>

      <a href="index.html">
      <input class="btn btn-primary" type="button" value="Done" style="display: block; margin-left: auto; margin-right: auto; margin-top: 50px;"></a>
    </div>
  </div> <!---end of row-->
</div> <!---end of container 2-->




<!---Validation-->
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

</body>
</html>