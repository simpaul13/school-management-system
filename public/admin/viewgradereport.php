<?php require_once("../../resources/config.php");

		$query = query("SELECT * FROM grade");

		confirm($query);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Teachers Portal</title>

	<!---CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<!---JS-->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  	<link rel="stylesheet" type="text/css" href="takeAtt.css">
</head>
<body>

<!---Content-->
	<!---First table-->
	<h5 style="text-align: center;">KNOWLEDGE</h5>
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col"></th>
	      <th scope="col">Quizzes</th>
	      <th scope="col">Monthly Test (MT)</th>
	      <th scope="col">Periodical Test (PT)</th>
	      <th scope="col">Average</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="col"></th>
	      <td>5</td>
	      <td>5</td>
	      <td>5</td>
	      <td>11</td>
	    </tr>
	    <tr>
	      <th scope="row"></th>
	      <td>5</td>
	      <td></td>
	      <td></td>
	      <td></td>
	    </tr>
	    <tr>
	      <th scope="row"></th>
	      <td>5</td>
	      <td></td>
	      <td></td>
	      <td></td>
	    </tr>
	  </tbody>
	</table>

	<!---Second table-->
	<h5 style="text-align: center;">SKILLS/UNDERSTANDING</h5>
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col"></th>
	      <th scope="col">Homework</th>
	      <th scope="col">Performances</th>
	      <th scope="col">Average</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="col"></th>
	      <td>5</td>
	      <td>5</td>
	      <td>3.3000000000000003
</td>
	    </tr>
	    <tr>
	      <th scope="row"></th>
	      <td></td>
	      <td>5</td>
	      <td></td>
	    </tr>
	    <tr>
	      <th scope="row"></th>
	      <td></td>
	      <td>5</td>
	      <td></td>
	    </tr>
	  </tbody>
	</table>

	<!---Third table-->
	<h5 style="text-align: center;">PRODUCT/PERFORMANCE</h5>
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col"></th>
	      <th scope="col">OP</th>
	      <th scope="col">PR</th>
	      <th scope="col">AT</th>
	      <th scope="col">School Activity</th>
	      <th scope="col">Average</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="col"></th>
	      <td>5</td>
	      <td>5</td>
	      <td>5</td>
	      <td>5</td>
	      <td>8.55</td>
	    </tr>
	    <tr>
	      <th scope="row"></th>
	      <td></td>
	      <td></td>
	      <td></td>
	      <td>5</td>
	      <td></td>
	    </tr>
	    <tr>
	      <th scope="row"></th>
	      <td></td>
	      <td></td>
	      <td></td>
	      <td>5</td>
	      <td></td>
	    </tr>
	  </tbody>
	</table>

</body>
</html>