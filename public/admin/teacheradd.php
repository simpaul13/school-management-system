<?php require_once("../../resources/config.php");?>
<?php
if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Teacheradd</title>

  <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style3.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->

    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/ionicons@4.2.5/dist/ionicons.js"></script>

</head>
<body>

  <div class="col-sm-12">
    <br><br>
       <form class="needs-validation" method="POST" novalidate>
        <h3>Add Teacher</h3>
    <?php addteacher(); ?>
                <!---form-row 1-->
                  <div class="form-row">
                    <!---Firstname form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">First name</label>
                      <input name="firstname" type="text" class="form-control" id="validationCustom01" placeholder="First Name" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the firstname field is required
                      </div>
                    </div> <!---end of Firstname form-->
                  <!---Register date form-->
                    <div class="col-md-5 mb-3" style="margin-left: 90px;">
                      <label for="validationCustom01">Register date</label>
                      <input name="registerdate" type="date" class="form-control" id="validationCustom01" placeholder="Register date" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Register date field is required
                      </div>
                    </div> <!---end of Register date form-->
                  </div> <!---end of form-row 1-->
                <!---form-row 2-->
                  <div class="form-row">
                    <!---Lastname form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Last name</label>
                      <input name="lastname" type="text" class="form-control" id="validationCustom01" placeholder="Last Name" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Lastname field is required
                      </div>
                    </div> <!---end of Lastname form-->
                  <!---Select job type form-->
                    <label for="validationCustom01" style="margin-left: 100px;">Job type</label>
                    <select name="jobtype" id="validationCustom01" class="custom-select" style="width: 200px; margin-left: -60px; margin-top: 32px;" required>
                        <option value="">Select Job type</option>
                        <option value="Full-time">Full-time</option>
                        <option value="Part-time">Part-time</option>
                      </select>
                      <div class="invalid-feedback" style="margin-left: 440px; margin-top: -35px;">
                      choose job type
                      </div>
                  </div> <!---end of form-row 2-->
              <!---Form-row 2.1-->
                  <div class="form-row">
                    <!---Lastname form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Middle name</label>
                      <input name="middlename" type="text" class="form-control" id="validationCustom01" placeholder="Middle Name" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Lastname field is required
                      </div>
                    </div> <!---end of Lastname form-->
                  </div>
                <!---form-row 3-->
                  <div class="form-row">
                    <!---Date of Birth form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Date of Birth</label>
                      <input name="dateofbirth" type="date" class="form-control" id="validationCustom01" placeholder="Date of Birth" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Date of Birth field is required
                      </div>
                    </div> <!---end of Date of Birth form-->
                  </div> <!---end of form-row 3-->
                <!---form-row 4-->
                  <div class="form-row">
                    <!---Age form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Age</label>
                      <input name="age" type="text" class="form-control" id="validationCustom01" placeholder="Age" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Age field is required
                      </div>
                    </div> <!---end of Age form-->
                  </div> <!---end of form-row 4-->
                <!---form-row 5-->
                  <div class="form-row">
                    <!---Contact form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Contact</label>
                      <input name="contact" type="text" class="form-control" id="validationCustom01" placeholder="Contact" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Contact field is required
                      </div>
                    </div> <!---end of Contact form-->
                  </div> <!---end of form-row 5-->
                <!---form-row 6-->
                   <div class="form-row">
                      <div class="col-md-4 mb-3" style="margin-left: 20px;">
                        <label for="validationDefaultUsername">Username</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2">@</span>
                          </div>
                          <input name="username" type="text" class="form-control" id="validationDefaultUsername" placeholder="Username" aria-describedby="inputGroupPrepend2" required>
                          <!---invalid feedback-->
                          <div class="invalid-feedback">
                            the username field is required
                          </div>
                        </div>
                      </div>
                    </div> <!---end of form-row 6-->
                                      <!---form-row 7-->
                  <div class="form-row">
                    <!---Address form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Password</label>
                      <input name="password" type="password" class="form-control" id="validationCustom01" placeholder="Password" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Password field is required
                      </div>
                    </div> <!---end of Address form-->
                  </div> <!---end of form-row 7-->
                  <!---form-row 7-->
                  <div class="form-row">
                    <!---Address form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">Address</label>
                      <input name="address" type="text" class="form-control" id="validationCustom01" placeholder="Address" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the Address field is required
                      </div>
                    </div> <!---end of Address form-->
                  </div> <!---end of form-row 7-->
                <!---form-row 8-->
                  <div class="form-row">
                    <!---City form-->
                    <div class="col-md-5 mb-3" style="margin-left: 20px;">
                      <label for="validationCustom01">City</label>
                      <input name="city" type="text" name="city" class="form-control" id="validationCustom01" placeholder="City" required>
                      <!---invalid feedback-->
                      <div class="invalid-feedback">
                        the City field is required
                      </div>
                    </div> <!---end of City form-->
                  </div> <!---end of form-row 8-->
                  <button class="btn btn-primary" name="submit" type="submit" style="float: right;">Save Information</button>
</form> <!---end of needs-validation-->
  </div>

</body>
</html>

  