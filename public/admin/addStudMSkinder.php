<?php require_once("../../resources/config.php");?>
<?php
if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>STUDENT MANAGEMENT SYSTEM</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style3.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/ionicons@4.2.5/dist/ionicons.js"></script>

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <?php require_once("../../resources/templates/front/menu_php.php");?>

<!-- Page Content  -->
     <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-info">
        <i class="fas fa-align-left"></i>
        </button>
        
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
           
        </div>
        </div>
    </nav>

    <!---Choose Strand card-->
    <!---Breadcrumb-->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="manageStud.php" style="color: blue; font-size: 15px;">
                manageStud
            </a></li>
            <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px;">
                Add Student 
            </li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header" style="font-size: 20px;">
            </div>
    <!---Card Body-->
     <!---Card Body-->
      <div class="card-body">
       <h5>Student Information&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Registration Information</h5><br>
    <!---start of student information-->
    <form class="needs-validation" method="POST" novalidate>
      <?php kindergarten(); ?>
      <!---form 1-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">First Name</label>                             
      <div class="col-sm-3">                 
        <input name="firstname" type="text" class="form-control" id="validationCustom01" id="myReset" placeholder="Firstname" maxlength="100" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the First Name field is required
        </div>
      </div>
      <!---form 1.2-->
      <label for="validationCustom01" class="col-sm-20 col-form-label" style="margin-left: 100px;">Registration Date</label> 
      <div class="col-sm-3" style="margin-left: 50px;"> 
        <input name="registrationdate" type="date" class="form-control" id="validationCustom01" placeholder="Registration Date" maxlength="100" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Registration Date field is required
        </div>
      </div> <!---end of form 1.2-->
    </div> <!---end of form 1-->
    <!---form 1.3-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Middle Name</label>                             
      <div class="col-sm-3">                 
        <input name="middlename" type="text" class="form-control" id="validationCustom01" placeholder="Middlename" maxlength="100" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Middle field is required
        </div>
      </div>
      <!---form 1.3.1-->
      <label for="validationCustom01" class="col-sm-2 col-form-label" style="margin-left: 130px;">Strand</label>
      <div class="col-sm-3">
        <select name="strand" id="validationCustom01" class="custom-select" required>
            <option value="">Select Strand</option>
            <option value="Nursery">Nursery</option>
            <option value="Kindergarten">Kindergarten</option>
            <option value="Preparatory">Preparatory</option>
          </select>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          select strand
        </div>
      </div> <!---end of form 1.3.1-->
    </div> <!---end of form 1.3-->
    <!---form 2-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Last Name</label>
      <div class="col-sm-3">
        <input name="lastname" type="text" class="form-control" id="validationCustom01" placeholder="Lastname" maxlength="100" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Last Name field is required
        </div>
      </div>
      <!---form 2.1-->
      <label for="validationCustom01" class="col-sm-2 col-form-label" style="margin-left: 130px;">Section</label>
      <div class="col-sm-3">
        <select name="section" id="validationCustom01" name="Section" class="custom-select" required>
            <option value="">Select Section</option>
            <option value="Topaz">Topaz</option>
            <option value="Sapphire">Sapphire</option>
            <option value="Opal">Opal</option>
            <option value="Diamond">Diamond</option>
            <option value="Pearl">Pearl</option>
            <option value="Ruby">Ruby</option>
          </select>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          select strand
        </div>
      </div> <!---end of form 2.1-->
    </div> <!---end of form 2-->
    <!---form 3-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Date of Birth</label>
      <div class="col-sm-3">
        <input type="date" name="dateofbirth" class="form-control" id="validationCustom01" placeholder="Date of Birth" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Date of Birth field is required
        </div>
      </div>
    </div> <!---end of form 3-->
    <!---form 4-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Age</label>
      <div class="col-sm-3">
        <input type="number" name="age" class="form-control" id="validationCustom01" placeholder="Age" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Age field is required
        </div>
      </div>
    </div> <!---end of form 4-->
    <!---form 5-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Gender</label>
      <div class="col-sm-3">
        <select id="validationCustom01" name="gender" class="custom-select" required>
            <option value="">Select Gender</option>
            <option value="Female">Female</option>
            <option value="Male">Male</option>
          </select>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          select gender
        </div>
      </div>
    </div> <!---end of form 5-->
    <!---form 6-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Parents/Guardian Contact Number</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="guardiancontactnumber" id="validationCustom01" placeholder="Contact" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Contact is required
        </div>
      </div>
    </div> <!---end of form 6-->
  <!---Start of address information-->
  <br>
  <h5 class="card-title">Address Information</h5><br>
  <!---form 7-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Address</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="address" id="validationCustom01" placeholder="Address" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Address is required
        </div>
      </div>
    </div> <!---end of form 7-->
    <!---form 8-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">City</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="city" id="validationCustom01" placeholder="City" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the City is required
        </div>
      </div>
    </div> <!---end of form 8-->
    <!---form 9-->
    <div class="form-group row">
      <label for="validationCustom01" class="col-sm-2 col-form-label">Country</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="country" id="validationCustom01" placeholder="Country" required>
        <!---invalid feedback-->
        <div class="invalid-feedback">
          the Country is required
        </div>
      </div>
    </div> <!---end of form 9-->
  <button onclick="myRefresh()" class="btn btn-light" type="reset" style="float: right; margin: 10px;">Reset</button>
  <button class="btn btn-primary" name="submit" type="submit" style="float: right; margin: 10px;">Save Information</button>
  </form> <!---end of form-->
</div>
</div>
</div>

    </div>

    <div class="overlay"></div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function () {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>

    <!---Refresh-->
    <script>
    function myRefresh() {
        location.reload();
    }
    </script>


    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
</body>

</html>