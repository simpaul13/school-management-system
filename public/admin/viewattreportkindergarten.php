<!DOCTYPE html>
<html>
<head>
	<title>Teachers Portal</title>

	<!---CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<!---JS-->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



  <link rel="stylesheet" type="text/css" href="takeAtt.css">
</head>
<body style="background-color: whitesmoke;">

<nav class="navbar" style="background-color: #191970;">
  <span class="navbar-brand mb-0 h1" style="color: white;">Student Management System</span>
  <div class="dropdown dropleft">
  <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
    Menu
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="#">Link 1</a>
    <a class="dropdown-item" href="#">Link 2</a>
    <a class="dropdown-item" href="#">Link 3</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">Log Out</a>
  </div>
</div>

</nav>
</nav>

<br>
<br>

<div class="container">
  <div class="row">
    <div class="col-sm-4" style="background-color: #adff2f; padding: 20px;">
      <img src="icon/profile.svg" width="20px">
      &emsp;Attendance Report
    </div>
  </div>
</div>

<div class="background">

<!---Second-->
 <div class="col-sm-12 padding-8 top-100" id="background2">
      <div class="card ">
        <div class="card-body">

          <!---Alert bar-->
          <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;" id="alert">
            <strong>Submit Successful!
              <img src="icon/checked.svg" width="15px"></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <!---Table-->
           <table class="table">
              <thead class="table-warning">
                <br>

                <tr>
                  <th scope="col">Student No.</th>
                  <th scope="col">Student Name</th>
                  <th scope="col"></th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>
                    <a onclick="newWindow()" class="btn btn-dark" role="button" style="color: white;">View</a>
                  </td>
                  <td>
                    <a class="btn btn-dark" href="#" role="button" data-toggle="modal" data-target="#exampleModalCenter">Submit</a>
                  </td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>
                    <a class="btn btn-dark" href="#" role="button">View</a>
                  </td>
                  <td>
                    <a class="btn btn-dark" href="#" role="button">Submit</a>
                  </td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Larry</td>
                  <td>
                    <a class="btn btn-dark" href="#" role="button">View</a>
                  </td>
                  <td>
                    <a class="btn btn-dark" href="#" role="button">Submit</a>
                  </td>
                </tr>
              </tbody>
            </table>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <img src="icon/question-mark.svg" width="20px">
            </div>
            <div class="modal-body">
              Are you sure you want to submit?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <button data-dismiss="alert" aria-label="Close" type="button" class="btn btn-primary" onclick="myAlert()">Yes</button>
            </div>
          </div>
        </div>
      </div>
      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>

</div>





<!---Javascript-->
<!---Show second Container-->
<script type="text/javascript">
  function myFunction() {
    var x = document.getElementById("background2");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

<!---Show Alert-->
<script type="text/javascript">
  function myAlert() {
    var x = document.getElementById("alert");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

<!---Refresh-->
    <script>
    function myRefresh() {
        location.reload();
    }
    </script>

 <script>
        var myVar;

        function myFunction() {
            myVar = setTimeout(showPage, 1000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        }
  </script>

  <!---New window-->
  <script>
    function newWindow() {
        window.open("viewattreport.html", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
    }
  </script>

</body>
</html>