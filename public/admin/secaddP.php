<?php require_once("../../resources/config.php");?>
<?php
if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

 
  <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Font Awesome JS -->
 <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/ionicons@4.2.5/dist/ionicons.js"></script>

	<meta charset="UTF-8">
	<title>Add</title>
</head>
<body>

	<div class="row">
		<div class="col-sm-4">
			
		</div>		
		<div class="col-sm-4">

			<form method="POST">
				<?php secaddP(); ?>
				<label for="validationCustom01">Section name</label>

      			<input name="sectionname" type="text" class="form-control" placeholder="Section Name">

				<label for="validationCustom01">Teacher name</label>

				<select name="teachername" class="form-control">
					<option>Select Teacher</option>
					<?php teacheroption();?>
				</select>
				<br>
				<input name="submit" class="btn btn-primary" type="submit" value="Submit">
			</form>

		</div>		
		<div class="col-sm-4">
			
		</div>
	</div>

</body>
</html>