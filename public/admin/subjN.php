<?php require_once("../../resources/config.php");?>
<?php
if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>STUDENT MANAGEMENT SYSTEM</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style3.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/ionicons@4.2.5/dist/ionicons.js"></script>

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
   <?php require_once("../../resources/templates/front/menu_php.php");?>

<!-- Page Content  -->
     <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-info">
        <i class="fas fa-align-left"></i>
        </button>
        
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
           
        </div>
        </div>
    </nav>

    <!---Choose Strand card-->
    <!---Breadcrumb-->
         <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="subj.html" style="color: blue; font-size: 15px;">
                Class Subject
            </a></li>
            <li class="breadcrumb-item active" style="font-size: 15px;">
                Choose Strand 
            </li>
            <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px;">
                Nursery 
            </li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header" style="font-size: 20px;">
            Kindergarten Subject
        </div>

    <!---Card Body-->
    <div class="card-body">

         <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" style="float: right; margin: 10px; font-size: 16px;">
        <span class="glyphicon">&#x2b;</span>
            Add Subject
        </button>
        <?php display_message();?>
        <!---Table-->
        <table class="table table-striped">
          <thead>
            <tr class="table-info">
              <th scope="col">Subject Name</th>
              <th scope="col">Teacher Name</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <?php subjn();?>
          </tbody>
        </table>


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Section</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
<!---MODAL CONTENT-->
    <div class="modal-body">
    <form class="needs-validation" method="POST" novalidate>
    <div class="form-row">
      <?php  subjNadd();?>
    <!---Section name form-->
         <div class="col-md-12 mb-3">
            <label for="validationCustom01">Subject Name</label>
            <input type="text" name="subjectname" class="form-control" id="validationCustom01" placeholder="Subject Name" maxlength="10" required>
            <!---valid feedback-->
            <div class="valid-feedback">
                success!
            </div>
            <!---invalid feedback-->
            <div class="invalid-feedback">
                section name is required
            </div>
            </div> <!---end of col-md-12 mb-3-->

            <!---Select teacher name form-->
            <div class="col-md-12 mb-3">
             <label for="validationCustom01">Teacher name</label><br>
             <select id="validationCustom01" name="teachername" class="custom-select" required>
                <option value="">Choose Teacher</option>
                <option value="Teacher Bern">Teacher Bern</option>
                <option value="Teacher Leny">Teacher Leny</option>
                <option value="Teacher Joy">Teacher Joy</option>
                </select>
                <!---invalid feedback-->
                <div class="invalid-feedback">
                    Choose teacher's name
                </div>
                <!---valid feedback-->
                <div class="valid-feedback">
                    Success!
                </div>
                </div> <!---end of col-md-12 mb-3-->
                </div> <!---end of form row-->
                <button class="btn btn-primary" name="submit" type="submit" style="float: right;">
                    Add Section
                </button>
                </form> <!---end of needs-validation-->
              </div> <!---end of form-row-->
              <div class="modal-footer">
                <button onclick="myRefresh()" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
              </div> <!---end of modal content-->
            </div>
          </div>
        </div>
    </div>
    <div class="line"></div>
</div>
</div>
<div class="overlay"></div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <!---Refresh-->
    <script>
    function myRefresh() {
        location.reload();
    }
    </script>

    <!---Validation-->
     <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function () {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
</body>

</html>