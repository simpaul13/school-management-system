<?php require_once("../../resources/config.php");?>
<?php 

if(!isset($_SESSION['username'])) {


redirect("../../public");

}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ATTENDACE MANAGEMENT SYSTEM</title>
    

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style3.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/ionicons@4.2.5/dist/ionicons.js"></script>

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <?php require_once("../../resources/templates/front/menu_php.php");?>


<!-- Page Content  -->
     <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-info">
        <i class="fas fa-align-left"></i>
        </button>
        
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
           
        </div>
        </div>
    </nav>

    <!---Choose Strand card-->
    <!---Breadcrumb-->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="teacher.php" style="color: blue; font-size: 15px;">
                Teacher
            </a></li>
            <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px;">
               Manage Teacher
            </li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header" style="font-size: 20px;">
            Attendace Report
            </div>

    <!---Card Body-->
            <center>
              <h3><p><?php display_message(); ?></p></h3>
            </center>
      

            
    <div class="card-body">

      


<div class="background">

<!---Second-->
 <div class="col-sm-12 padding-8 top-100" id="background2">
      <div class="card ">
        <div class="card-body">

            <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">1st Grading</a>
            </div>
          </nav>

        <!---first tab-->
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
          
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a onclick="newWindow()" class="btn btn-dark" role="button" style="color: white;">View</a>
              </td>
              
            </tr>
          </tbody>
        </table>
      </div> <!---end of first tab-->

      <!---Second tab-->
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>

            <tr>
              <th scope="row">2</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>

            <tr>
              <th scope="row">3</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>
          </tbody>
        </table>
      </div> <!---end of first tab-->
        
        <!---Third tab-->
          <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>d
          </tbody>
        </table>
      </div> <!---end of first tab-->

        <!---Fourth tab-->
          <div class="tab-pane fade" id="nav-fourth" role="tabpanel" aria-labelledby="nav-fourth-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr> 
          </tbody>
        </table>
      </div> <!---end of first tab-->
            <div class="tab-pane fade" id="nav-Oct" role="tabpanel" aria-labelledby="nav-Oct-tab"></div>
            <div class="tab-pane fade" id="nav-Nov" role="tabpanel" aria-labelledby="nav-Nov-tab"></div>
            <div class="tab-pane fade" id="nav-Dec" role="tabpanel" aria-labelledby="nav-Dec-tab"></div>
            <div class="tab-pane fade" id="nav-Jan" role="tabpanel" aria-labelledby="nav-Jan-tab"></div>
            <div class="tab-pane fade" id="nav-Feb" role="tabpanel" aria-labelledby="nav-Feb-tab"></div>
            <div class="tab-pane fade" id="nav-March" role="tabpanel" aria-labelledby="nav-March-tab"></div>
            <div class="tab-pane fade" id="nav-April" role="tabpanel" aria-labelledby="nav-April-tab"></div>
          </div>
      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>

</div>
        
    </div>
</div>
</div>


    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <!---Refresh-->
      <script>
    function newWindow() {
        window.open("viewgradereport.php", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
    }
  </script>
    <script>
    function myRefresh() {
        location.reload();
    }
    </script>

    <!---Validation-->
     <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function () {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });

         function AddingSection() {     
          window.open(
            "viewgradereport.php","_blank", 
            "toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=200,width=900,height=500"); 
        }    

        function checkDelete(){
    return confirm('Are you sure?');
}

</script>
</body>

</html>