<!DOCTYPE html>
<html>
<head>
  <title>Teachers Portal</title>

  <!---CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!---JS-->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



  <link rel="stylesheet" type="text/css" href="takeAtt.css">
</head>
<body>

<div class="col-sm-12 padding-8 top-100" id="background2">
      <div class="card ">
        <div class="card-body">

            <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">June</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">July</a>
              <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">August</a>
              <a class="nav-item nav-link" id="nav-Sept-tab" data-toggle="tab" href="#nav-Sept" role="tab" aria-controls="nav-Sept" aria-selected="false">September</a>
              <a class="nav-item nav-link" id="nav-Oct-tab" data-toggle="tab" href="#nav-Oct" role="tab" aria-controls="nav-Oct" aria-selected="false">October</a>
              <a class="nav-item nav-link" id="nav-Nov-tab" data-toggle="tab" href="#nav-Nov" role="tab" aria-controls="nav-Nov" aria-selected="false">November</a>
              <a class="nav-item nav-link" id="nav-Dec-tab" data-toggle="tab" href="#nav-Dec" role="tab" aria-controls="nav-Dec" aria-selected="false">December</a>
              <a class="nav-item nav-link" id="nav-Jan-tab" data-toggle="tab" href="#nav-Jan" role="tab" aria-controls="nav-Jan" aria-selected="false">January</a>
              <a class="nav-item nav-link" id="nav-Feb-tab" data-toggle="tab" href="#nav-Feb" role="tab" aria-controls="nav-Feb" aria-selected="false">February</a>
              <a class="nav-item nav-link" id="nav-March-tab" data-toggle="tab" href="#nav-March" role="tab" aria-controls="nav-March" aria-selected="false">March</a>
              <a class="nav-item nav-link" id="nav-April-tab" data-toggle="tab" href="#nav-April" role="tab" aria-controls="nav-April" aria-selected="false">April</a>
            </div>
          </nav>


          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Mon<br>4</th>
                    <th scope="col">Tue<br>5</th>
                    <th scope="col">Wed<br>6</th>
                    <th scope="col">Th<br>7</th>
                    <th scope="col">F<br>8</th>
                    <th scope="col">Sat<br>9</th>
                    <th scope="col">Sun<br>10</th>
                    <th scope="col">Sun<br>11</th>
                    <th scope="col">Sun<br>12</th>
                    <th scope="col">Sun<br>13</th>
                    <th scope="col">Sun<br>14</th>
                    <th scope="col">Sun<br>15</th>
                    <th scope="col">Sun<br>16</th>
                    <th scope="col">Sun<br>17</th>
                    <th scope="col">Sun<br>18</th>
                    <th scope="col">Sun<br>19</th>
                    <th scope="col">Sun<br>20</th>
                    <th scope="col">Sun<br>21</th>
                    <th scope="col">Sun<br>22</th>
                    <th scope="col">Sun<br>23</th>
                    <th scope="col">Sun<br>24</th>
                    <th scope="col">Sun<br>25</th>
                    <th scope="col">Sun<br>26</th>
                    <th scope="col">Sun<br>27</th>
                    <th scope="col">Sun<br>28</th>
                    <th scope="col">Sun<br>29</th>
                              </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1"></label>
                      </div>
                    </td>
                    <td></td>
                    <td>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1"></label>
                      </div>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
            </table>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
            <div class="tab-pane fade" id="nav-Sept" role="tabpanel" aria-labelledby="nav-Sept-tab">...</div>
            <div class="tab-pane fade" id="nav-Oct" role="tabpanel" aria-labelledby="nav-Oct-tab"></div>
            <div class="tab-pane fade" id="nav-Nov" role="tabpanel" aria-labelledby="nav-Nov-tab"></div>
            <div class="tab-pane fade" id="nav-Dec" role="tabpanel" aria-labelledby="nav-Dec-tab"></div>
            <div class="tab-pane fade" id="nav-Jan" role="tabpanel" aria-labelledby="nav-Jan-tab"></div>
            <div class="tab-pane fade" id="nav-Feb" role="tabpanel" aria-labelledby="nav-Feb-tab"></div>
            <div class="tab-pane fade" id="nav-March" role="tabpanel" aria-labelledby="nav-March-tab"></div>
            <div class="tab-pane fade" id="nav-April" role="tabpanel" aria-labelledby="nav-April-tab"></div>
          </div>
      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>

</div>

</body>
</html>
