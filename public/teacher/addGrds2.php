<?php require_once("../../resources/config.php");?>
<?php if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html>
<head>
  <title>Teachers Portal</title>

  <!---CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/laoding.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>


  <link rel="stylesheet" type="text/css" href="addGrds2.css">
</head>
<body style="background-color: whitesmoke;" onload="myLoading()">

<!--------------------NAVBAR--------------------->
<?php require_once("../../resources/templates/front/menu_teacher.php");?>

<br>
<br>

<div class="container">
  <div class="row">
    <div class="col-sm-4" style="background-color: #ffd700; padding: 20px;">
      <img src="icon/test.svg" width="20px">
      &emsp;Add Grades
    </div>
  </div>
</div>

<div class="background">

<!--------------------CONTENT
<div id="loader"></div>
<main style="display:none;" id="myDiv" class="animate-bottom">--------------------->

<!--------------------FIRST CARD--------------------->
<div class="col-sm-12 padding-8 top-100" id="background"  style="padding: 30px;">
      <div class="card ">
        <div class="card-body">

           <h5>KNOWLEDGE</h5>
           
           <div class="row">
           <div class="col-sm-6">
              <div class="card ">
                <div class="card-body">

                    <h5>Quizzes</h5>

                    <form method="POSTS">

                      <div class="form-row">
                        <div class="col">
                          <input id="Q1" type="text" name="q1" class="form-control" placeholder="#1">
                        </div>
                        <div class="col">
                          <input id="Q2" type="text" name="q2" class="form-control" placeholder="#2">
                        </div>
                        <div class="col">
                          <input id="Q3" type="text" name="q3" class="form-control" placeholder="#3">
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-12">
                          <h5>Total</h5>
                          <center>
                              <label id="QT">Total (15%)</label>
                          </center>
                        </div>

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="col">
                          <h5>MT</h5>
                          <input type="text" id="MT" name="mt" class="form-control">
                          <center>
                           	<br>
                          <label id="MT1">(10%)</label>                           
                          </center>
                        </div>
                        <div class="col">
                          <h5>PT</h5>
                          <input type="text" id="PT" name="pt" class="form-control">
                          <center>
                           	<br>
                          <label id="PT1">(25%)</label>                           
                          </center>
                        </div>

                      </div>
                   

                </div>
                    <!--End Card-body-->
              </div>
                    <!--End Card-->
            </div>

            <div class="col-sm-6">
              <div class="card ">
                <div class="card-body">

                    
                      <div class="form-row">
                        <div class="col-sm-12">
                          <h5>Average</h5>
                          <center>
                          <label id="MTP">Average (50%)</label>                           
                          </center>
                        </div>
                      </div>
                   
                </div>
                    <!--End Card-body-->
                <button  onclick="total()"  type="button" class="btn btn-primary">=</button>
              </div>
                    <!--End Card-->
            </div>
        </div> <!---end of row-->

      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>
      <!---end of first card-->

<!--------------------SECOND CARD--------------------->
<div class="col-sm-12 padding-8 top-100" id="background"  style="padding: 30px;">
      <div class="card ">
        <div class="card-body">

           <h5>SKILLS/UNDERSTANDING</h5>
           
           <div class="row">
           <div class="col-sm-6">
              <div class="card ">
                <div class="card-body">

                    <h5>Homework (5%)</h5>

                   
                      <div class="form-row">
                        <div class="col-sm-12">
                          <input type="text" id="H1" class="form-control" placeholder="#1">
                        </div>
                        <div class="col-sm-12">
                          <center>
                            <br>
                          <label id="H1T">(25%)</label>                           
                          </center>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-12">
                          <h5>Performances (15%)</h5>
                        </div>
                        <div class="col">
                          <input type="text" id="P1" class="form-control" placeholder="#1">
                        </div>
                        <div class="col">
                          <input type="text" id="P2" class="form-control" placeholder="#2">
                        </div>
                        <div class="col">
                          <input type="text" id="P3" class="form-control" placeholder="#3">
                        </div>
                        <div class="col-sm-12">
                          <center>
                            <br>
                          <label id="P123">(15%)</label>                           
                          </center>
                        </div>
                      </div>
                  

                </div>
                    <!--End Card-body-->
              </div>
                    <!--End Card-->
            </div>

            <div class="col-sm-6">
              <div class="card ">
                <div class="card-body">

                      <div class="form-row">
                        <div class="col-sm-12">
                          <h5>Average</h5>
                          <center>
                            <label id="A20">Average (20%)</label>
                          </center>
                        </div>
                      </div>
               
                </div>
                    <!--End Card-body-->
                    <button  onclick="total2()"  type="button" class="btn btn-primary">=</button>
              </div>
                    <!--End Card-->
            </div>
        </div> <!---end of row-->

      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>
      <!---end of second card-->

<!--------------------THIRD CARD--------------------->
<div class="col-sm-12 padding-8 top-100" id="background"  style="padding: 30px;">
      <div class="card ">
        <div class="card-body">

           <h5>PRODUCT/PERFORMACE</h5>
           
           <div class="row">
           <div class="col-sm-6">
              <div class="card ">
                <div class="card-body">

                              <div class="form-row">
                        <div class="col">
                          <h5>OP</h5>
                          <input type="text" id="OP" class="form-control" placeholder="OP">
                          <br>
                          <center>
                            <label id="OPT">(10%)</label>
                          </center>
                        </div>
                        <div class="col">
                          <h5>PR</h5>
                          <input type="text" id="PR" class="form-control" placeholder="PR">
                          <br>
                          <center>
                            <label id="PRT">(5%)</label>
                          </center>
                        </div>
                        <div class="col">
                          <h5>AT</h5>
                          <input type="text" id="AT" class="form-control" placeholder="AT%">
                          <br>
                          <center>
                            <label id="ATT">(5%)</label>
                          </center>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-12">
                          <h5>School Activity</h5>
                        </div>
                        <div class="col">
                          <input type="text" id="S1" class="form-control" placeholder="#1">
                        </div>
                        <div class="col">
                          <input type="text" id="S2" class="form-control" placeholder="#2">
                        </div>
                        <div class="col">
                          <input type="text" id="S3" class="form-control" placeholder="#3">
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-12">
                          <center>
                            <label id="S123">(10%)</label>
                          </center>
                        </div>
                      </div>
                    

                </div>
                    <!--End Card-body-->
              </div>
                    <!--End Card-->
            </div>

            <div class="col-sm-6">
              <div class="card ">
                <div class="card-body">

                   
                      <div class="form-row">
                        <div class="col-sm-12">
                          <h5>Average</h5>
                          <center>
                            <label id="A30">Average (30%)</label>
                          </center>
                        </div>
                      </div>
              

                </div>
                    <!--End Card-body-->
                    <button  onclick="total3()"  type="button" class="btn btn-primary">=</button>
              </div>
                    <!--End Card-->
            </div>
        </div> <!---end of row-->

      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>
      <!---end of second card-->

<!--------------------THIRD CARD--------------------->
<div class="col-sm-12" style="padding-left: 40%; padding-right: 40%;">
  <div class="card ">
    <div class="card-body">

      <div class="form-row">
        <div class="col-sm-12">
          <h5>TOTAL</h5>
          <center>
            <label id="final">100%</label>
          </center>
        </div>
      </div>
    

      </div>
        <!--End Card-body-->
      </div>
        <!--End Card-->
</div>

<br>
<br>
<br>
<!--------------------FOURTH CARD--------------------->
<div class="col-sm-12" style="padding-left: 30%; padding-right: 30%;">
  <div class="card ">
    <div class="card-body">
    
      <div class="form-row">
      <div class="col-sm-12">
          <button onclick="totalfinal()" type="button" class="btn btn-primary btn-lg btn-block">Calculate</button>
          <br>
          <button type="button" name="submit" class="btn btn-primary btn-lg btn-block">Save</button>
        </div>
      </div>
      </form>

      </div>
        <!--End Card-body-->
      </div>
        <!--End Card-->
</div>


</div>

</main>



<!---Javascript-->
<script type="text/javascript">
  function myFunction() {
    var x = document.getElementById("background2");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}



function total(){

  var Q1 = document.getElementById('Q1').value;
  var Q2 = document.getElementById('Q2').value;
  var Q3 = document.getElementById('Q3').value;
  var Qtotal = parseFloat(Q1)+parseFloat(Q2)+parseFloat(Q3) / 3 * 0.15;

  if(!isNaN(Qtotal)){
    document.getElementById("QT").innerHTML=Qtotal;
  }

  var MT = document.getElementById('MT').value;
  var MTtotal = parseFloat(MT) * 0.10;

  if(!isNaN(MTtotal)){
    document.getElementById("MT1").innerHTML=MTtotal;
  }

  var PT = document.getElementById('PT').value;
  var PTtotal = parseFloat(PT) * 0.10;

  if(!isNaN(PTtotal)){
    document.getElementById("PT1").innerHTML=PTtotal;
  }

  var QT = document.getElementById('QT').innerHTML;
  var MT1 = document.getElementById('MT1').innerHTML
  var PT1 = document.getElementById('PT1').innerHTML;
  var Average = parseFloat(QT) + parseFloat(MT1) + parseFloat(PT1) * 0.5;

  if(!isNaN(Average)){
    document.getElementById("MTP").innerHTML=Average;
  }
}

function total2(){

  var H1 = document.getElementById('H1').value;
  var H1Total = parseFloat(H1) * 0.25;

  if(!isNaN(H1Total)){
    document.getElementById("H1T").innerHTML=H1Total;
  }

  var P1 = document.getElementById('P1').value;
  var P2 = document.getElementById('P2').value;
  var P3 = document.getElementById('P3').value;
  var P123 = parseFloat(P1)+parseFloat(P2)+parseFloat(P3) / 3 * 0.15;

  if(!isNaN(P123)){
    document.getElementById("P123").innerHTML=P123;
  }

  var H1T = document.getElementById('H1T').innerHTML;
  var P123 = document.getElementById('P123').innerHTML
  var Average2 = parseFloat(H1T) + parseFloat(P123)* 0.2;

  if(!isNaN(Average2)){
    document.getElementById("A20").innerHTML=Average2;
  }
}

function total3(){

  var OP = document.getElementById('OP').value;
  var OPTotal = parseFloat(OP) * 0.10;

  if(!isNaN(OPTotal)){
    document.getElementById("OPT").innerHTML=OPTotal;
  }

  var PR = document.getElementById('PR').value;
  var PRTotal = parseFloat(PR) * 0.5;

  if(!isNaN(PRTotal)){
    document.getElementById("PRT").innerHTML=PRTotal;
  }

  var AT = document.getElementById('AT').value;
  var ATTotal = parseFloat(AT) * 0.5;

  if(!isNaN(ATTotal)){
    document.getElementById("ATT").innerHTML=ATTotal;
  }

  var S1 = document.getElementById('S1').value;
  var S2 = document.getElementById('S2').value;
  var S3 = document.getElementById('S3').value;
  var S123total = parseFloat(S1)+parseFloat(S2)+parseFloat(S3) / 3 * 0.10;

  if(!isNaN(S123total)){
    document.getElementById("S123").innerHTML=S123total;
  }

  var OPT = document.getElementById('OPT').innerHTML;
  var PRT = document.getElementById('PRT').innerHTML
  var ATT = document.getElementById('ATT').innerHTML
  var S123 = document.getElementById('S123').innerHTML
  var Average3 = parseFloat(OPT) + parseFloat(PRT) + parseFloat(ATT) + parseFloat(S123) * 0.30;

  if(!isNaN(Average3)){
    document.getElementById("A30").innerHTML=Average3;
  }
}

function totalfinal(){

  var MTP = document.getElementById('MTP').innerHTML;
  var A20 = document.getElementById('A20').innerHTML;
  var A30 = document.getElementById('A30').innerHTML;
  var finaltotal = parseFloat(MTP)+parseFloat(A20)+parseFloat(A30);

  if(!isNaN(finaltotal)){
    document.getElementById("final").innerHTML=finaltotal;
  }
}

</script>

</script>



<!---Refresh-->
    <script>
    function myRefresh() {
        location.reload();
    }
    </script>




<!---Loading
  <script>
      var myVar;

        function myLoading() {
            myVar = setTimeout(showPage, 1000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        }
  </script>-->


</body>
</html>