
<?php require_once("../../resources/config.php");?>
<?php if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Teachers Portal</title>

	<!---CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/laoding.css">
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body style="background-color: whitesmoke; margin:0;" onload="myFunction()">
<?php require_once("../../resources/templates/front/menu_teacher.php");?>


 <!--This will popup the loading....-->
        <div id="loader"></div>
        <main style="display:none;" id="myDiv" class="animate-bottom">
            <div class="row">
                <div class="col-sm-12">
                    <div class="center top-50">
                        
                    </div>
                </div>
                <div class="col-sm-6 padding-8 top-100">
                    <div class="card ">
                        <div class="card-body">
                        	<img src="icon/profile.svg" width="100px" height="100px"><br><br>
                            <a class="btn btn-success btn-lg btn-block" href="takeAtt.php" role="button">Take Attendance</a>
                            <button type="button" class="btn btn-outline-success btn-lg btn-block"><a href="attreport.php" style="text-decoration: none; color: black;">Attendance Report</a></button>
                        </div>
                        <!--End Card-body-->
                    </div>
                    <!--End Card-->
                </div>
                <!--End sm-col-6 -->
                <div class="col-sm-6 padding-8 top-100">
                    <div class="card">
                        <div class="card-body">
                        	<img src="icon/test.svg" width="100px" height="100px"><br><br>
                            <a class="btn btn-success btn-lg btn-block" href="addTable.php" role="button">Add Grades</a>
                            <button type="button" class="btn btn-outline-success btn-lg btn-block"><a href="gradestable.php" style="text-decoration: none; color: black;">Manage Grade Report</a></button>
                        </div>
                        <!--End Card-body-->
                    </div>
                    <!--End Card-->
                </div>
                <!--End sm-col-6 -->
            </div>
            <!--End row-->
        </main>
        <script>
        var myVar;

        function myFunction() {
            myVar = setTimeout(showPage, 1000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        }
        </script>


</body>
</html>