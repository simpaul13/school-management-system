<?php require_once("../../resources/config.php");?>
<?php if(!isset($_SESSION['username'])) {


redirect("../../public");

}


 ?>
<!DOCTYPE html>
<html>
<head>
  <title>Teachers Portal</title>

  <!---CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/laoding.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>


  <link rel="stylesheet" type="text/css" href="addGrds2.css">
</head>
<body style="background-color: whitesmoke;" onload="myLoading()">

<?php require_once("../../resources/templates/front/menu_teacher.php");?>

<br>
<br>

<div class="container">
  <div class="row">
    <div class="col-sm-4" style="background-color: #ffd700; padding: 20px;">
      <img src="icon/test.svg" width="20px">
      &emsp;Student Grades Report
    </div>
  </div>
</div>

<div class="background">

<!---Second-->
<div id="loader"></div>
<main style="display:none;" id="myDiv" class="animate-bottom">
 <div class="col-sm-12 padding-8 top-100" id="background2">
      <div class="card ">
        <div class="card-body">

            <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">1st Grading</a>
            </div>
          </nav>

        <!---first tab-->
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a onclick="newWindow()" class="btn btn-dark" role="button" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>
          </tbody>
        </table>
      </div> <!---end of first tab-->

      <!---Second tab-->
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>

            <tr>
              <th scope="row">2</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>

            <tr>
              <th scope="row">3</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>
          </tbody>
        </table>
      </div> <!---end of first tab-->
        
        <!---Third tab-->
          <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr>d
          </tbody>
        </table>
      </div> <!---end of first tab-->

        <!---Fourth tab-->
          <div class="tab-pane fade" id="nav-fourth" role="tabpanel" aria-labelledby="nav-fourth-tab">
        <!---Table-->
        <table class="table">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Learner's Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>
                <a class="btn btn-dark" role="button" target="_blank" style="color: white;">View</a>
              </td>
              <td>
                <input class="btn btn-dark" type="submit" value="Submit">
              </td>
            </tr> 
          </tbody>
        </table>
      </div> <!---end of first tab-->
            <div class="tab-pane fade" id="nav-Oct" role="tabpanel" aria-labelledby="nav-Oct-tab"></div>
            <div class="tab-pane fade" id="nav-Nov" role="tabpanel" aria-labelledby="nav-Nov-tab"></div>
            <div class="tab-pane fade" id="nav-Dec" role="tabpanel" aria-labelledby="nav-Dec-tab"></div>
            <div class="tab-pane fade" id="nav-Jan" role="tabpanel" aria-labelledby="nav-Jan-tab"></div>
            <div class="tab-pane fade" id="nav-Feb" role="tabpanel" aria-labelledby="nav-Feb-tab"></div>
            <div class="tab-pane fade" id="nav-March" role="tabpanel" aria-labelledby="nav-March-tab"></div>
            <div class="tab-pane fade" id="nav-April" role="tabpanel" aria-labelledby="nav-April-tab"></div>
          </div>
      </div>
      <!--End Card-body-->
    </div>
      <!--End Card-->
 </div>

</div>

</main>



<!---Javascript-->
<script type="text/javascript">
  function myFunction() {
    var x = document.getElementById("background2");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

<!---Refresh-->
    <script>
    function myRefresh() {
        location.reload();
    }
    </script>

<!---Loading-->
  <script>
      var myVar;

        function myLoading() {
            myVar = setTimeout(showPage, 1000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        }
  </script>

  <!---New window-->
  <script>
    function newWindow() {
        window.open("viewgradereport.php", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=150,left=500,width=400,height=400");
    }
  </script>


</body>
</html>