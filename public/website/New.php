<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SCHOOL MANAGEMENT SYSTEM</title>

	<!---ICON LINK PLUGIN-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!---CSS PLUGINS-->
	<link rel="stylesheet" type="text/css" href="info1.css">
</head>
<body>

	<!---Input Box-->
	<div id="new">
		<!---Search Box-->
		<div class="first-layer">
			<div class="search">
				<span style="color: whitesmoke;">Search Student Name</span>
				<input type="search" name="search" class="searchlay" placeholder="type name here">
			</div>
		</div>
		<!---Input Box second layer-->
		<!---Left-->
		<div class="center-content">
		<div class="second-layer">
			<div class="ID">
				<span><b>Student ID:</b></span><br>
	    		<input type="text" id="myTxt" name="StudentID" class="StdntID" min="1" maxlength="30" required="" disabled>
			</div>
			<div class="name">
				<span><b>First Name:</b></span><br>
	    		<input type="text" id="myTxt-1" name="Firstname" class="firstname" min="1" maxlength="30" required disabled>
			</div>
			<div class="name">
				<span><b>Last Name:</b></span><br>
	    		<input type="text" id="myTxt-2" name="Lastname" class="firstname" min="1" maxlength="30" required disabled>
			</div>
			<div class="name">
				<span><b>Middle Name:</b></span><br>
	    		<input type="text" id="myTxt-3" name="Middlename" class="firstname" min="1" maxlength="30" required disabled>
			</div>
		</div>
		<!---Center-->
		<div class="third-layer">
			<div class="selectage">
				<span><b>Age:</b></span><br>
	    		<input type="number" id="myTxt-4" name="Age" class="age" disabled>
			</div>
			<!---Radio button-->
			<div class="radio">
			<input type="radio" id="myTxt-5" value="Male" name="gender" disabled><b>Female</b>&nbsp;
			<input type="radio" id="myTxt-6" value="Female" name="gender" disabled><b>Male</b>
			</div>
			<div class="birthdate">
				<span><b>Birthdate:</b></span><br>
	    		<input type="month" id="myTxt-7" name="Birthdate" class="birth" required disabled>
			</div>
			<div class="birthplace">
				<span><b>Birthplace:</b></span><br>
	    		<input type="text" id="myTxt-8" name="Birthplace" class="place" min="1" maxlength="30" required disabled>
			</div>
			<div class="religion">
				<span><b>Religion:</b></span><br>
	    		<input type="text" id="myTxt-9" name="Religion" class="religion-1" min="1" maxlength="30" required disabled>
			</div>
		</div>
		<!---Right-->
		<div class="fourth-layer">
			<div class="father">
				<span><b>Father's Name:</b></span><br>
	    		<input type="text" id="myTxt-10" name="Fathersname" class="fthrname" min="1" maxlength="30" required disabled>
			</div>
			<div class="mother">
				<span><b>Mother's Name:</b></span><br>
	    		<input type="text" id="myTxt-11" name="Mothersname" class="mthrname" min="1" maxlength="30" required disabled>
			</div>
			<div class="address">
				<span><b>Present Address:</b></span><br>
	    		<input type="text" id="myTxt-12" name="PresentAddress" class="present" min="1" maxlength="30" required disabled>
			</div>
		</div>
		<div class="fifth-layer">
			<div class="number">
				<span><b>Contact Number:</b></span><br>
	    		<input type="tel" id="myTxt-13" name="Contactnumber" class="contact" maxlength="11" required disabled>
			</div>
			<div class="strands">
				<span><b>Grade Level:</b></span><br>
	    		<select class="level" id="myTxt-14" name="Select" disabled>
	    			<option>Nursery</option>
				    <option>Kinder</option>
				    <option>Preparatory</option>
	    		</select>
			</div>
			<div class="time">
				<span><b>Time:</b></span><br>
	    		<input type="text" id="myTxt-15" name="Time" class="sched" maxlength="20" required disabled>
			</div>
		</div>
		<!---Right button center-->
		<div class="btn-center">
			<button>Save</button><br><br>
			<button>Update</button><br><br>
			<button>Delete</button>
		</div>
		</div>
	</div>
	<!---Button footer-->
	<div class="btn-footer">
		<button class="btn-1" onclick="undisableTxt()" style="color: white;"><i class="fa fa-child"></i>&emsp;Add New</button>
		<button class="btn-1" onclick="myReload()"><a href="New.php" style="text-decoration: none; color: white;"><i class="fa fa-refresh fa-spin"></i>&emsp;Refresh</a></button>
		<button class="btn-1"><a href="stdntinfo.php" style="text-decoration: none; color: white;"><i class="fa fa-arrow-circle-o-left"></i>&emsp;Back</a></button>
	</div>

	<!---JAVASCRIPT-->
		<script type="text/javascript">
			function undisableTxt() {
				document.getElementById("myTxt").disabled = false;
				document.getElementById("myTxt-1").disabled = false;
				document.getElementById("myTxt-2").disabled = false;
				document.getElementById("myTxt-3").disabled = false;
				document.getElementById("myTxt-4").disabled = false;
				document.getElementById("myTxt-5").disabled = false;
				document.getElementById("myTxt-6").disabled = false;
				document.getElementById("myTxt-7").disabled = false;
				document.getElementById("myTxt-8").disabled = false;
				document.getElementById("myTxt-9").disabled = false;
				document.getElementById("myTxt-10").disabled = false;
				document.getElementById("myTxt-11").disabled = false;
				document.getElementById("myTxt-12").disabled = false;
				document.getElementById("myTxt-13").disabled = false;
				document.getElementById("myTxt-14").disabled = false;
				document.getElementById("myTxt-15").disabled = false;
			}
		</script>

		<!---Reload-->
		<script type="text/javascript">
			function myReload() {
				location.reload(true);
			}
		</script>
	
</body>
</html>