<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title>SCHOOL MANAGEMENT SYSTEM</title>
  
	<!---ICON LINK PLUGIN-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!---CSS PLUGIN-->
	<link rel="stylesheet" type="text/css" href="stdntinfo1.css">
</head>
<body onload="startTime()">

	<!---NAVBAR-->
    <!---NAVBAR-->
    <div id="nav">
      <img src="img/girl.png" alt="img" class="pic">
      <p class="wc">Welcome Teacher Bern<br>Time:&emsp;<span id="txt"></span>&emsp;Date:</p>
    </div>

    <!---CONTENT-->
    <div id="content">
    	<!---Button-->
    	<div class="top">
    		<button class="btn"><a href="New.php" style="text-decoration: none;  color: black;"><i class="fa fa-child"></i>&emsp;New</a></button>
    		<button class="btn"><a href="#" style="text-decoration: none;  color: black;"><i class="fa fa-close"></i>&emsp;Drop</a></button>
    		<button class="btn"><a href="#" style="text-decoration: none;  color: black;"><i class="fa fa-close"></i>&emsp;Delete</a></button>
    	<!---Search-->
    	<span style="font-family: calibri; color: whitesmoke; font-size: 14px;">Search Student Name</span>
       <span class="search-box">
         <input class="search-txt" type="text" name="" placeholder="Type to search">
       		<a class="search-btn" href="#" style="text-decoration: none;">
         	<i class="fa fa-search"></i>
       		</a>
       </span>
    	</div>
    </div>

    <!---Footer-->
    <!---Button-->
     <div class="btn-footer">
        <button class="btn-1" onclick="myReload()"><a href="stdntinfo.php" style="text-decoration: none; color: white;"><i class="fa fa-refresh fa-spin"></i>&emsp;Refresh</a></button>
        <button class="btn-1"><a href="school.php" style="text-decoration: none; color: white;"><i class="fa fa-arrow-circle-o-left"></i>&emsp;Cancel</a></button>
    </div>

	<!---JAVASCRIPT-->
      <!---Time & Date-->
    <script>
        function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
        }
    </script>


</body>
</html>