<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>SCHOOL MANAGEMENT SYSTEM</title>

    <!---ICON LINK PLUGIN-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!---CSS PLUGIN-->
    <link rel="stylesheet" type="text/css" href="sec.css">
</head>
<body onload="startTime()">

	<!---NAVBAR-->
    <!---NAVBAR-->
    <div id="nav">
      <img src="img/girl.png" alt="img" class="pic">
      <p class="wc">Welcome Teacher Bern<br>Time:&emsp;<span id="txt"></span>&emsp;Date:</p>
    </div>

    <!---CONTENT-->
    <div id="content">
    	<!---Button-->
    	<div class="top">
    		<span class="nme">Manage Section</span>
    		<button class="btn" style="color: black;"><i class="fa fa-trash"></i>&emsp;Delete</button>
    		<button class="btn" style="color: black;"><i class="fa fa-history"></i>&emsp;Update</a></button>
    		<button class="btn" id="myBtn" style="color: black;" disabled="disabled"><i class="fa fa-save"></i>&emsp;Save</a></button>
    	</div>

    <!---Input box-->
    <div class="center-content">
		<div class="second-layer">
			<div class="strands">
				<span><b>Grade Level:</b></span><br>
	    		<select class="level" id="myTxt-14" name="Select" required="required">
	    			<option>Nursery</option>
				    <option>Kinder</option>
				    <option>Preparatory</option>
	    		</select>
			</div>
			<div class="name">
				<span><b>Section Name:</b></span><br>
	    		<input type="text" id="myTxt-1" name="Section Name" class="sec" min="1" maxlength="30" required>
			</div>
		</div>
	</div>
</div>

    <!---Footer-->
    <!---Button-->
     <div class="btn-footer">
        <button class="btn-1" onclick="myReload()"><a href="sec.php" style="text-decoration: none; color: white;"><i class="fa fa-refresh fa-spin"></i>&emsp;Refresh</a></button>
        <button class="btn-1"><a href="school.php" style="text-decoration: none; color: white;"><i class="fa fa-arrow-circle-o-left"></i>&emsp;Cancel</a></button>
    </div>

	<!---JAVASCRIPT-->
      <!---Time & Date-->
    <script>
        function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
        }
    </script>

</body>
</html>