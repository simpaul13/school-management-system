<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>SCHOOL MANAGEMENT SYSTEM</title>

    <!---ICON LINK PLUGIN-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!---CSS PLUGIN-->
    <link rel="stylesheet" type="text/css" href="history2.css">
</head>
<body>

	<div id="new">
		<!---Input box-->
		<div class="nav">
			<span class="year">2018-2019</span>
			<span class="grd">&emsp;<i class="fa fa-search-plus"></i>&emsp;Search Student Name:</span>&nbsp;
			<input type="search" name="search" class="grade">
		</div>]
	</div>

	<div class="btn">
		<button class="btn-1" onclick="myReload()"><a href="New.php" style="text-decoration: none; color: white;"><i class="fa fa-refresh fa-spin"></i>&emsp;Refresh</a></button>
		<button class="btn-1"><a href="enrollhistory.php" style="text-decoration: none; color: white;"><i class="fa fa-arrow-circle-o-left"></i>&emsp;Back</a></button>
	</div>

	<!---Reload-->
		<script type="text/javascript">
			function myReload() {
				location.reload(true);
			}
		</script>

</body>
</html>