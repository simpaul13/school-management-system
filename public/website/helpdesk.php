<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FASC CREATIVE LEARNING SCHOOL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	 <!---BOOTSTRAP PLUGINS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
    <!---CSS PLUGINS-->
	<link rel="stylesheet" href="helpdesk.css">
</head>
<body>

<!---HEADER-->
<!---NAV BAR--> 
   <nav class="navbar navbar-default" style="border-bottom: 4px solid darkblue; background-color: white; -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25); ">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    <!---Logo-->
      <a class="navbar-brand" href="FASC.html"><img src="img/logo.png" alt="" style="position: relative; width: 40px; top: -10px; "></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active" ><a href="FASC.html" data-toggle="tooltip" title="Home" style="background-color: darkblue; font-family: calibri; color: white;">HOME<span class="sr-only">(current)</span></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" title="About" style="color: black; font-family: calibri;">ABOUT<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="admin.html">FASC Founders</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="MV.html">Mission &amp; Vision</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">School Facilities</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="fasclife.html">FASC life</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="uniform.html">Uniform</a></li>
          </ul>
        </li>
         <li><a href="program.html" style="color: black; font-family: calibri;" data-toggle="tooltip" title="Programs">PROGRAMS</a></li>
         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: black; font-family: calibri;" data-toggle="tooltip" title="Admission">ADMISSION<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="EGuidelines.html">Enrollment Guidelines</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Tuition Fees</a></li>
          </ul>
        </li>
        <li><a href="#" style="color: black; font-family: calibri;" data-toggle="tooltip" title="Contact Us">CONTACT US</a></li>
      </ul>
    <!---Search bar-->  
      <form class="navbar-form navbar-left" action="/action_page.php">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
      </div>
    </form>
     <!---Login-->
          <ul class="nav navbar-nav navbar-right">
          <li><a href="helpdesk.php" style="color: black; font-family: calibri;"><span class="glyphicon glyphicon-comment"></span>&nbsp;FASC HelpDesk</a></li>
          <li><a href="login.php" style="color: black; font-family: calibri;"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>


<!---CONTENT-->
	<div class="container" style="background-color: white; height: 800px;">
		<div id="content" style="font-family: calibri; margin: 50px;">
			<h3 style="font-size: 40px;"><strong>CONTACT US</strong></h3>
      <p>please enter your message below.<br>
        items marked with a * are required</p><br>
			<span><b>Firstname:*</b></span><br>
			<input type="text" name="firstname" class="first" min="1" maxlength="30" required="required" placeholder="first name">
			<br><br><span class="middle"><b>Middlename:</b></span><br>
			<input type="text" name="middlename" class="middle" id="input-middle" min="1" maxlength="30" required="required" placeholder="middle name">
			<br><br><span class="last"><b>Lastname:*</b></span><br>
			<input type="text" name="lastname" class="last" id="input-last" min="1" maxlength="30" required="required" placeholder="last name">
			<span class="email"><b>Email:*</b></span><br>
			<input type="text" name="email" class="email" id="input-email" min="1" maxlength="30" required="required" placeholder="Email Address">
			<span class="contact"><b>Contact Number:*</b></span><br>
			<input type="text" name="contact number" class="contact" id="input-contact" min="1" maxlength="30" required="required" placeholder="Landline or Mobile Number">
			<br><span class="current"><b>Current Address:</b></span><br>
			<input type="text" name="current" class="current" id="input-current" min="1" maxlength="30" required="required" placeholder="Location">
			<span><b>I would like to...*</b></span><br>
	    		<select class="select" name="Select">
	    			<option>Please select subject</option>
				    <option>Inquire about your programs and tuition fees</option>
				    <option>Request for academic records</option>
	    			<option>Request for academic verification</option>
				    <option>Apply for a job position in your company</option>
				    <option>Make suggestions</option>
            <option>Make complaint</option>
            <option>Ask something else</option>
	    		</select>
		  <span class="Message"><b>Message:*</b></span><br>
			<input type="text" name="current" class="Message" id="input-Message" min="1" maxlength="30" required="required">
      <br>
      <button type="submit" class="submit">Submit</button>
		</div>
	</div>

<!---FOOTER-->
 <!---SOCIAL MEDIA-->
    <div id="pricing" class="container-fluid" id="social" style="background: url(img/green2.jpg); background-size: cover; font-family: calibri;">
    <div class="row slideanim" style="position: relative; top: 10px;">
    <div class="col-sm-4 col-xs-12">
      <div class="panel" style="background-color: transparent;">
        <div class="panel-body">
          <div class="fb-page" data-href="https://www.facebook.com/FascCreativeLearningSchoolInc" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore" style="height: 30px;"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
        </div>
      </div>      
    </div>     
    <div class="col-sm-4 col-xs-12">
      <div class="panel" style="background-color: transparent;">
        <div class="panel-body">
         <div class="text-center center-block">
            <p class="txt-railway"><span style="font-family: calibri; color: goldenrod; font-size: 40px;">Keep in touch</span></p>
            <br />
                <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
              <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
              <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
              <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
             <br>
             <br>
             <br>
             <br>
             <p><span style="color: goldenrod; font-family: calibri; font-size: 30px;">Quick Links</span><br>
             <br>
             <a href="MV.html" title="Visit w3schools" style="color: white; font-size: 18px;">MISSION &amp; VISION</a><br>
             <br>
             <a href="#" title="Visit w3schools" style="color: white; font-size: 18px;">SCHOOL FACILITES</a><br>
             <br>
             <a href="#" title="Visit w3schools" style="color: white; font-size: 18px;">UNIFORM</a><br>
             <br>
             <a href="EGuidelines.html" title="Visit w3schools" style="color: white; font-size: 18px;">ENROLLMENT GUIDELINES</a><br>
             <br>
             <a href="#" title="Visit w3schools" style="color: white; font-size: 18px;">TUITION FEE</a>
             </p>
            </div>
            </div>
          </div>
        </div>
    <div class="col-sm-4 col-xs-12">
        <p class="txt-railway"><span style="font-family: calibri; color: goldenrod; font-size: 35px; position: relative; top: 50px;">VISIT US</span></p>
       <div class="panel text-center" style="position: relative; top: 30px; background-color: transparent;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d241.1560035597883!2d121.1526106668718!3d14.74105999639154!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397bb48a39de4b5%3A0x1663e3072127e939!2sFasc+Creative+Learning+School%2C+Inc.!5e0!3m2!1sen!2sph!4v1520766379446" width="300" height="250" frameborder="0" style="border:0; position: relative; top: 50px;"></iframe>
        </div> 
    </div>
    </div>
    </div>             
     
<!---FOOTER-->
   <div style="background-color: darkgreen; height: 170px; color: antiquewhite;">
       <footer class="container-fluid text-center" >
      <p style="position: relative; top: 40px;">Copyright &copy; FASC Creative Learning School, Inc. All Rights Reserved</p>
      <ol class="breadcrumb" style="position: relative; top: 50px; background-color: transparent;">
      <li style="color: black;">HTML5</li>
      <li style="color: black;">CSS3</li>
       <li style="color: black;">BOOTSTRAP3</li>
       </ol>  
    </footer>
   </div>   
    </div>
    </div>
    </div>             
    
<!---JAVASCRIPT-->
   <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>