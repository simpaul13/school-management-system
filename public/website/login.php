<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>FASC CREATIVE LEARNING SCHOOL</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!---ICON LINK PLUGIN-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!---CSS PLUGIN-->
	<link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>

<!---CONTENT-->
	<div class="loginBox">
		<img class="user" src="img/girl.png">
		<h3>Sign in here</h3>
		<form action="school.php">
			<div class="inputBox">
				<input type="text" name="" placeholder="Username">
				<span><i class="fa fa-user"></i></span>
			</div>
			<div class="inputBox">
				<input type="password" name="" placeholder="Password">
				<span><i class="fa fa-lock"></i></span>
			</div>
			<!---Button-->
			<a href="school.php">
				<input type="submit" name="" value="Login">
			</a>
		</form>
		<a href="#">Forget Password</a>
		<br>
		<a href="FASC.html">Cancel</a>
	</div>

</body>
</html>