<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SCHOOL MANAGEMENT SYSTEM</title>
	<!---ICON LINK PLUGIN-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!---CSS PLUGIN-->
	<link rel="stylesheet" type="text/css" href="sched3.css">
</head>
<body onload="startTime()">

	<!---NAVBAR-->
    <div id="nav">
      <img src="img/girl.png" alt="img" class="pic">
      <p class="wc">Welcome Teacher Bern<br>Time:&emsp;<span id="txt"></span>&emsp;Date:</p>
    </div>

    <!---CONTENT-->
    <div id="content">
    	<!---Button-->
    	<div class="top">
    		<span class="nme">Schedule Controller</span>
    		<button class="btn" style="color: black;"><i class="fa fa-trash"></i>&emsp;Delete</button>
    		<button class="btn" style="color: black;"><i class="fa fa-history"></i>&emsp;Update</a></button>
    		<button class="btn" id="myBtn" style="color: black;" disabled="disabled"><i class="fa fa-save"></i>&emsp;Save</a></button>
    	</div>
    	<!---Input forms-->
    	<!---First layer-->
    	<div class="center-content">
    	<div class="first-layer">
			<div class="year">
				<span style="font-size: 14px;"><b>Academic year:</b></span><br>
	    		<input class="in" id="myTxt" type="text" name="StudentID" min="1" maxlength="30" placeholder="2017-2018" required="" disabled>
			</div><br>
			<div class="strands">
				<span style="font-size: 14px;"><b>Level:</b></span><br>
	    		<select class="in" id="myTxt-1" name="Select" disabled>
	    			<option>Nursery</option>
				    <option>Kinder</option>
				    <option>Preparatory</option>
	    		</select>
			</div>
			<div class="sec">
				<span style="font-size: 14px;"><b>Section:</b></span><br>
	    		<select class="in" id="myTxt-2" name="Select" disabled>
	    			<option>Nursery-Topaz</option>
	    			<option>Nursery-Sapphire</option>
				    <option>Kindergarten-Opal</option>
				    <option>Kindergarten-Diamond</option>
				    <option>Preparatory-Ruby</option>
	    		</select>
			</div>
		</div>
		<!---Second layer-->
		<div class="second-layer">
			<div class="days">
				<span style="font-size: 14px;"><b>Day/s:</b></span><br>
				<span style="font-size: 14px;">Monday &amp; Tuesday</span>
	    		<input class="in" id="myTxt-3" type="checkbox" name="Subject" min="1" maxlength="30" required="required" disabled="disabled"><br>
	    		<span style="font-size: 14px;">Wednesday &amp; Thursday</span>
	    		<input class="in" id="myTxt-4" type="checkbox" name="Subject" min="1" maxlength="30" required="required" disabled="disabled"><br>
	    		<span style="font-size: 14px;">Friday</span>
	    		<input class="in" id="myTxt-5" type="checkbox" name="Subject" min="1" maxlength="30" required="required" disabled="disabled">
			</div><br>
			<div class="time">
				<span style="font-size: 14px;"><b>Time:</b></span><br>
	    		<input class="in" id="myTxt-6" type="text" name="Time" min="1" maxlength="30" required disabled>
			</div>
		</div>
		<!---Third layer-->
		<div class="third-layer">
			<div class="ad">
				<span style="font-size: 14px;"><b>Teacher:</b></span><br>
	    		<input class="in" id="myTxt-7" type="text" name="Teacher" min="1" maxlength="30" required disabled>
			</div><br>
			<div class="subj">
				<span style="font-size: 14px;"><b>Subject:</b></span><br>
	    		<input class="in" id="myTxt-8" type="text" name="Subject" min="1" maxlength="30" required="required" disabled="disabled">
			</div>
		</div>
    	</div>
    </div> <!---end of content-->

    <!---Button footer-->
	<div class="btn-footer">
		<button class="btn-1" onclick="undisableTxt()" style="color: white;"><i class="fa fa-child"></i>&emsp;Add New</button>
		<button class="btn-1" onclick="myReload()"><a href="sched.php" style="text-decoration: none; color: white;"><i class="fa fa-refresh fa-spin"></i>&emsp;Refresh</a></button>
		<button class="btn-1"><a href="school.php" style="text-decoration: none; color: white;"><i class="fa fa-arrow-circle-o-left"></i>&emsp;Cancel</a></button>
	</div>
	
    <!---JAVASCRIPT-->
      <!---Time & Date-->
    <script>
        function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
        }
    </script>

    <script type="text/javascript">
    	function undisableTxt() {
    		document.getElementById("myTxt").disabled = false;
				document.getElementById("myTxt-1").disabled = false;
				document.getElementById("myTxt-2").disabled = false;
				document.getElementById("myTxt-3").disabled = false;
				document.getElementById("myTxt-4").disabled = false;
				document.getElementById("myTxt-5").disabled = false;
				document.getElementById("myTxt-6").disabled = false;
				document.getElementById("myTxt-7").disabled = false;
				document.getElementById("myTxt-8").disabled = false;
				document.getElementById("myBtn").disabled = false;
    	}
    </script>
</body>
</html>