<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SCHOOL MANAGEMENT SYSTEM</title>

	<!---CSS PLUGIN-->
	<link rel="stylesheet" type="text/css" href="enrollhistory.css">
</head>
<body>

	<!---NAVBAR-->
    <div id="nav">
      <img src="img/girl.png" alt="img" class="pic">
      <p class="wc">Welcome Teacher Bern<br>Time:&emsp;<span id="txt"></span>&emsp;Date:</p>
    </div>

    <!---CONTENT-->
    <div id="content">
    	<button class="button1"><a href="history1.php" style="text-decoration: none; color: black;">&emsp;&emsp;2017-2018&emsp;&emsp;</a></button><br>
    	<button class="button1"><a href="history2.php" style="text-decoration: none; color: black;">&emsp;&emsp;2018-2019&emsp;&emsp;</a></button><br>
    	<button class="button3"><a href="school.php" style="text-decoration: none; color: black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CANCEL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></button>
    </div>
    

    <!---JAVASCRIPT-->
      <!---Time & Date-->
    <script>
        function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
        }
    </script>

</body>
</html>