<!DOCTPYE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="Style/style.css">
	<title></title>
</head>
<body onload="startTime()">

<!--Image-->
<div class="input-group image">
    <img src="image/avatar.png" alt="profile" width="90" height="90">
	<img src="image/logo2.png" alt="profile" width="800" height="90" style="position: relative; top: 10px; left: 100px;">
</div>
<!--Time & Date-->
<div class="form-group">
    <div class="">
        <div>Time:</div>
        <div id="txt"></div>
    </div>
    <div class="">
        <div>Date:</div>
        
    </div>
</div>
<!--List Group-->
<div class="form-group">
  
<div class="row">
 
  <div class="col-2">
    <div class="list-group" id="list-tab" role="tablist">
     
    <a class="list-group-item list-group-item-action active" id="list-Evaluation-list" data-toggle="list" href="#list-Evaluation" role="tab" aria-controls="Evaluation">STUDENT</a>
    
    <a class="list-group-item list-group-item-action" id="list-Gades-list" data-toggle="list" href="#list-Gades" role="tab" aria-controls="Gades">GRADING</a>
    
    <a class="list-group-item list-group-item-action" id="list-GreditSubject-list" data-toggle="list" href="#list-GreditSubject" role="tab" aria-controls="GreditSubject">CLASS LIST</a>
    
    <a class="list-group-item list-group-item-action" id="list-Maintenance-list" data-toggle="list" href="#list-Maintenance" role="tab" aria-controls="Maintenance">SCHEDULING</a>
    
    <a class="list-group-item list-group-item-action" id="list-StudentiInformation-list" data-toggle="list" href="#list-StudentiInformation" role="tab" aria-controls="StudentiInformation">ENROLLMENT HISTORY</a>
    
    <a class="list-group-item list-group-item-action" id="list-Course-list" data-toggle="list" href="#list-Course" role="tab" aria-controls="Course">STUDENTS RECORDS</a>
    
    <a class="list-group-item list-group-item-action" id="list-Course-list" data-toggle="list" href="#list-Course" role="tab" aria-controls="Course">SCHOOL YEAR</a>
    
    <a class="list-group-item list-group-item-action" id="list-AcademicYear-list" data-toggle="list" href="#list-AcademicYear" role="tab" aria-controls="AcademicYear">STRANDS</a>
    
    <a class="list-group-item list-group-item-action" id="list-Records-list" data-toggle="list" href="#list-Records" role="tab" aria-controls="Records">SECTION</a>
    
    </div>
  </div>
  
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
     <!--Evaluation-->
      <div class="tab-pane fade show active" id="list-Evaluation" role="tabpanel" aria-labelledby="list-Evaluation-list">
        <label></label> 
      
      <div class="form-group">
        
         <!--Evaluation Top-->
          <div class="input-group">
          
           <!--Input-One-->
            <div class="input-group Academic">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Academic Year</span>
                </div>
                <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="2017-2018" disabled>
            </div>
            
            <!--Input-Two-->
            <div class="input-group Grading">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Grading Period</span>
                </div>
                <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="1st Grading"disabled>
            </div>
			<!---Input-three-->
			
			</div>
          </div>
      </div>
      </div>
      <!--Gades-->
      <div class="tab-pane fade" id="list-Gades" role="tabpanel" aria-labelledby="list-Gades-list">Gades</div>
    
        
      <!--Subject-->
      <div class="tab-pane fade" id="list-GreditSubject" role="tabpanel" aria-labelledby="list-GreditSubject-list">Gredit Subject</div>
      
      
      <!--Maintenance-->
      <div class="tab-pane fade" id="list-Maintenance" role="tabpanel" aria-labelledby="list-Maintenance-list">Maintenance</div>
      
      
      <!--Information-->
      <div class="tab-pane fade" id="list-StudentiInformation" role="tabpanel" aria-labelledby="list-StudentiInformation-list">Student Information</div>
      
      
      <!--Course-->
      <div class="tab-pane fade" id="list-Course" role="tabpanel" aria-labelledby="list-Course-list">Course</div>
      
      
      <!--....-->
      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">...</div>
      
      
      <!--Academic Year-->
      <div class="tab-pane fade" id="list-AcademicYear" role="tabpanel" aria-labelledby="list-AcademicYear-list">Academic Year</div>
      
      
      <!--Record-->
      <div class="tab-pane fade" id="list-Records" role="tabpanel" aria-labelledby="list-Records-list">Records</div>
    </div>
  </div>
  
</div>


<script>
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
$(document).ready(function(){
    $("#myBtn3").click(function(){
        $("#myModal3").modal({backdrop: "static"});
    });
});
</script>
</body>
</html>


