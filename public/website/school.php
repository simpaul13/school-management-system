<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title>SCHOOL MANAGEMENT SYSTEM</title>
	<link rel="stylesheet" type="text/css" href="school.css">
</head>
<body onload="startTime()">

	<!---NAVBAR-->
    <div id="nav">
      <img src="img/girl.png" alt="img" class="pic">
      <p class="wc">Welcome Teacher Bern<br>Time:&emsp;<span id="txt"></span>&emsp;Date:</p>
    </div>
  <!---CONTENT-->
    <!---LOGO-->
		<img src="img/logo.png" alt="logo" class="logo">
    <!---BUTTON-->
    <div id="button-left">
      <button><a href="stdntinfo.php" class="btn-1">Student Information</a></button>
      <button><a href="grading.html" class="btn-2">Grading</a></button>
      <br>
      <br>
      <button><a href="classlist-teacher.html" class="btn-3">Class List<br>&amp; Teacher</a></button>
      <button><a href="sched.php"  class="btn-4">&emsp;Scheduling&emsp;&emsp;</a></button>
    </div>
    <div id="button-right">
      <button><a href="enrollhistory.php" class="btn-5">Enrollment History</a></button>
      <button><a href="sec.php" class="btn-6">&emsp;Section&emsp;</a></button>
      <br>
      <br>
      <button><a href="#" class="btn-7">Subject<br>&amp; School Year</a></button>
      <button><a href="#" class="btn-8">Student Records</a></button>
    </div>

    <!---JAVASCRIPT-->
      <!---Time & Date-->
    <script>
        function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
        }
    </script>

</body>
</html>