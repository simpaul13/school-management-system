<?php require_once("../resources/config.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>ONLINE REGISTRATION</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <link rel="stylesheet" type="text/css" href="registerHome.css">

</head>
<body>

<nav class="navbar" style="background-color: #006600; color: white;">
  <span class="navbar-brand mb-0 h1">ONLINE REGISTRATION</span>
</nav>


    <form method="POST">
      
<!---CONTENT-->
<div class="container">
  <div class="row">

      <div class="col-sm-12" style="margin-top: 50px; background-color: whitesmoke; padding: 30px; height: 800px;">
        
        <span><b>NOTE:</b></span> <p><i>Those have red asterisk <span style="color: red;">*</span> must filled up.</i></p>
        <h4>STUDENT INFORMATION</h4>

        <br>
        
          <?php register(); ?>
          <div class="form-row">

            <div class="col-md-7 mb-3">
              <label for="validationCustom01">Child's Name<span style="color: red;">*</span></label>
              <input name="childname" type="text" class="form-control" id="validationCustom01" placeholder="Last Name, First Name, Middle Name"required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col">
              <label for="validationCustom01">Gender<span style="color: red;">*</span></label>
              <select class="custom-select" name=gender>
                <option selected>Choose...</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>

              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col">
              <label for="validationCustom01" name="age">Age<span style="color: red;">*</span></label>
              <input type="number" name="age"class="form-control" id="validationCustom01" placeholder="Age"required>

              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

          </div>

          <div class="form-row">
            <div class="col-md-3">
              <label for="validationCustom01">Birth Date<span style="color: red;">*</span></label>
              <input type="date" name="birthdate" class="form-control" id="validationCustom01" placeholder="Birthdate"required>

              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col-md-6">
              <label for="validationCustom01">Birth Place<span style="color: red;">*</span></label>
              <input type="text" name="birthplace" class="form-control" id="validationCustom01" placeholder="Birthdate"required>

              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col">
              <label for="validationCustom01">Religion<span style="color: red;">*</span></label>
              <input type="text" name="religion" class="form-control" id="validationCustom01" placeholder="Birthdate"required>

              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

          </div> <!----end of form row 2-->
        
        <br>
        <br>
        <h4>PARENTS INFORMATION</h4>

        <br>
          <div class="form-row">

            <div class="col-md-8">
              <label for="validationCustom01">Father's Name<span style="color: red;">*</span></label>
              <input type="text" name="fathername" class="form-control" id="validationCustom01" placeholder="Full Name"required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col">
              <label for="validationCustom01">Contact Number<span style="color: red;">*</span></label>
              <input type="number" name="contactfather" class="form-control" id="validationCustom01" placeholder="Telephone/Cellphone"required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

          </div> <!---end of form 1-->

          <br>
          <div class="form-row">

            <div class="col-md-8 mb-3">
              <label for="validationCustom01">Mother's Name<span style="color: red;">*</span></label>
              <input type="text" name="mothername"class="form-control" id="validationCustom01" placeholder="Full Name"required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col">
              <label for="validationCustom01">Contact Number<span style="color: red;">*</span></label>
              <input type="number" name="contactmother" class="form-control" id="validationCustom01" placeholder="Telephone/Cellphone"required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>

            <div class="col-md-12 mb-3">
              <label for="validationCustom01">Present Address<span style="color: red;">*</span></label>
              <input type="text" name="presentaddress" class="form-control" id="validationCustom01" placeholder="Full Name"required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>


          </div> <!---end of form 2-->
      </div> <!---end of col-sm-12-->
    </div> <!---end of row-->
</div> <!---end of container-->

<!---second container-->
<div class="container">
  <div class="row">
    <div class="col-sm-4" style="margin-top: 50px; background-color: whitesmoke; padding: 30px; text-align: center;">
      <h5>APPLICATION FOR: <span style="color: red;">*</span></h5>
      <br>
      <br>
      <select name="application" class="custom-select">
        <option selected>Choose...</option>
        <option value="Nursery">Nursery</option>
        <option value="Kingdergarten">Kindergarten</option>
        <option value="Preparatory">Preparatory</option>
        <option value="Grade 1">Grade 1</option>
      </select>
    </div>

    <div class="col-sm-4" style="margin-top: 50px; background-color: whitesmoke; padding: 30px; text-align: center;">
      <h5>STUDENT STATUS: <span style="color: red;">*</span></h5>
      <br>
      <br>
      <select name="studentstatus" class="custom-select">
        <option selected>Choose...</option>
        <option value="New">New</option>
        <option value="Old">Old</option>
        <option onclick="enable()" value="Transferee">Transferee</option>
      </select>

      <br>
      <br>
      <br>
      <h5 style="font-size: 20px;">IF TRANSFEREE:</h5>
      <br>
      <label for="validationCustom01" style="float: left;">Last School Attended: (Optional)</label>
      <input type="text" name="iftransferee" class="form-control" id="validationCustom01" required>
      <div class="valid-tooltip">
        Looks good!
      </div>
    </div>

    <div class="col-sm-4" style="margin-top: 50px; background-color: whitesmoke; padding: 30px; text-align: center;">
      <h5>MODE OF PAYMENT: <span style="color: red;">*</span></h5>
      <br>
      <br>
      <select name="payment" class="custom-select">
        <option selected>Choose...</option>
        <option value="Cash">Cash</option>
        <option value="Installment">Installment</option>
      </select>
      <br>
      <br>
      <label for="validationCustom01" style="float: left;">Miscellaneous:</label>
      <input type="text" class="form-control" id="validationTooltip01" value="3,000" disabled="">
      <div class="valid-tooltip">
        Looks good!
      </div>
      <br>
      <label for="validationCustom01" style="float: left;">Books:</label>
      <input type="text" class="form-control" id="validationTooltip01" value="300" disabled="">
      <div class="valid-tooltip">
        Looks good!
      </div>
      <br>
      <label for="validationCustom01" style="float: left;">Uniform (Daily/P.E):</label>
      <input type="text" class="form-control" id="validationTooltip01" value="1000" disabled="">
      <div class="valid-tooltip">
        Looks good!
      </div>

      <button name="submit" type="submit" class="btn btn-primary"  role="button" style="margin: 50px;">Done</button>
    </div>
  </div> <!---end of row-->
</div> <!---end of container 2-->

    </form>


</body>
</html>