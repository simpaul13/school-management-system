<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <title>Login</title>
</head>

<body>
    <header>
    </header>
    <!--MAIN CONTENT-->
    <main>
        <!--Content-->
        <div class="container">
            <div class="row">
                <!--TITLE-->
                <div class="col-sm-12">
                    <div class="textcenter">
                        <h1>Log In</h1>
                    </div>
                </div>
                <!--ADMIN-->
                <div class="col-sm-6">
                    <div class="textcenter table">
                    	<br>
                        <h5>ADMIN</h5>
                       	
                       	<form method="POST" action="login.php">
                       		<button>Log In</button>
                       		<input type="text" name="username" placeholder="username">
                       		<input type="password" name="password" placeholder="password">
                       		
                       	</form>
                    </div>
                </div>
                <!--TEACHE4-->
                <div class="col-sm-6">
                    <div class="textcenter table">
                    	<br>
                        <h5>TEACHER</h5>
                        
                        <form method="POST" action="login.php">
                       		<button>Log In</button>
                       		<input type="text" name="username" placeholder="username">
                       		<input type="password" name="password" placeholder="password">
                        </form>
                    </div>
                </div>
            </div>
    </main>
    <footer>
    </footer>
   
</body>

</html>