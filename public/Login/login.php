<?php require_once("../../resources/config.php");?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Log In</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/laoding.css">
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <body>
        <main>
            <div class="row">
                <div class="col-sm-12">
                    <div class="top-50">
                        <center>
                            <h1>Log In</h1>
                        </center>
                    </div>
                </div>
                <div class="col-sm-6 padding-8 top-100">
                    <div class="card ">
                        <div class="card-body">
                            <center>
                                <img src="icon/admin.svg" width="100px" height="100px">
                            </center>
                            <br>
                            <br>
                            <a href="admin.php">
                            	<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#Admin">Log In As Admin</button>
                            </a>
                        </div>
                        <!--End Card-body-->
                    </div>
                    <!--End Card-->
                </div>
                <!--End sm-col-6 -->
                <div class="col-sm-6 padding-8 top-100">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <img src="icon/school-teacher-table.svg" width="100px" height="100px">
                            </center>
                            <br>
                            <br>
                            <a href="teacher.php">
                            	 <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#Teacher">Log In As Teacher</button>
                            </a>
                        </div>
                        <!--End Card-body-->
                    </div>
                    <!--End Card-->
                </div>
                <!--End sm-col-6 -->
            </div>
            <!--End row-->
        </main>
    </body>

</html>