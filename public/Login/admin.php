<?php require_once("../../resources/config.php");?>
<?php login_admin(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
	<title>Admin</title>
</head>
<body>
<div class="row">
	<div class="col-sm-12 padding-10 top-50">
		<h1 class="center-text">Log In For Admin</h1>
		<form method="POST">
  			<div class="form-group">
    			<label>Username</label>
   				<input name="username" type="text" class="form-control" placeholder="Username">
 			 </div>
 			 <div class="form-group">
    			<label>Password</label>
   				<input name="password" type="password" class="form-control"  placeholder="Password">
  			</div>
  			<button name="submit" type="submit" class="btn btn-primary">Log In</button>
  			<a href="index.php">Back</a>
		</form>
	</div>
</div>

</body>
</html>