        <nav id="sidebar">
            <div id="dismiss">
                <i class="fas fa-arrow-left"></i>
            </div>

            <div class="sidebar-header">
                <img src="img/bern.jpg" class="img-thumbnail" alt="Cinque Terre" style="width: 100px;">
                <div class="pull-left info">
                    <br>
                    <p style="color: white;"><b>Bernadette N. Mira</b></p>
                </div>
            </div>

            <ul class="list-unstyled components">
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        Class
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="sec.php">Class Section</a>
                        </li>
                        <li>
                            <a href="subj.php">Class Subject</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        Student
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="manageStud.php">Manage Student</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="teacher.php">
                        Teacher
                    </a>
                </li>
               <li>
                    <a href="#attSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        Attendance
                    </a>
                    <ul class="collapse list-unstyled" id="attSubmenu">
                        <li>
                            <a href="attendance.php">Attendance report</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#gradeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        Grade
                    </a>
                    <ul class="collapse list-unstyled" id="gradeSubmenu">
                        <li>
                            <a href="grade.php">Manage Student grades</a>
                        </li>
                    </ul>
                </li>
                 <li>
                    <a href="register.php">
                        Register
                    </a>
                   
                </li> 
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="http://localhost/school/public/admin/logout.php" class="download">LOG OUT</a>
                </li>
            </ul>
        </nav>