<?php  

/*************************************************************************************************/
/******************************************HELPER CODE********************************************/
/*************************************************************************************************/


function redirect($location){
	header("Location: $location ");
}


function query($sql){

	global $connection;
		
	return mysqli_query($connection, $sql);

}

function confirm($result){
	global $connection;

	if (!$result){
		die("QUERY FAILED" . mysqli_error($connection));
	}

}

function escape_string($string){

	global $connection;

	return mysqli_real_escape_string($connection, $string);

}

function fetch_array($result){

	return mysqli_fetch_array($result);
}


function set_message($msg){

if(!empty($msg)) {

$_SESSION['message'] = $msg;
} else {
$msg = "";
    }
}


function display_message() {

    if(isset($_SESSION['message'])) {

        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}



/*************************************************************************************************/
/****************************************END OF THE LINE******************************************/
/*************************************************************************************************/


/*************************************************************************************************/
/*****************************Outputing data to the website***************************************/
/*************************************************************************************************/

/***********************************************UPDATE*************************************************/

#teacher
function update_teacher() {


if(isset($_POST['update'])) {


$firstname            = escape_string($_POST['firstname']);
$middlename           = escape_string($_POST['middlename']);
$lastname             = escape_string($_POST['lastname']);
$dateofbirth          = escape_string($_POST['dateofbirth']);
$age                  = escape_string($_POST['age']);
$contact              = escape_string($_POST['contact']);
$username             = escape_string($_POST['username']);
$password             = escape_string($_POST['password']);
$address              = escape_string($_POST['address']);
$registerdate         = escape_string($_POST['registerdate']);
$jobtype              = escape_string($_POST['jobtype']);
$city                 = escape_string($_POST['city']);



$query = "UPDATE teacher SET ";
$query .= "firstname            = '{$firstname}'       , ";
$query .= "middlename           = '{$middlename}'      , ";
$query .= "lastname             = '{$lastname}'        , ";
$query .= "dateofbirth          = '{$dateofbirth}'     , ";
$query .= "age                  = '{$age}'             , ";
$query .= "contact              = '{$contact}'         , ";
$query .= "username             = '{$username}'        , ";
$query .= "password             = '{$password}'        , ";
$query .= "address              = '{$address}'         , ";
$query .= "registerdate         = '{$registerdate}'    , ";
$query .= "jobtype              = '{$jobtype}'         , ";
$query .= "city                 = '{$city}'              ";
$query .= "WHERE idteacher=" . escape_string($_GET['id']);





$send_update_query = query($query);
confirm($send_update_query);
set_message("Teacher has been updated");
redirect("http://localhost/school/public/admin/teacher.php");


        }


}

#Grade1

function update_grade1() {


if(isset($_POST['update'])) {


$firstname                    = escape_string($_POST['firstname']);
$middlename                   = escape_string($_POST['middlename']);
$lastname                     = escape_string($_POST['lastname']);
$dateofbirth                  = escape_string($_POST['dateofbirth']);
$age                          = escape_string($_POST['age']);
$gender                       = escape_string($_POST['gender']);
$guardiancontactnumber        = escape_string($_POST['guardiancontactnumber']);
$address                      = escape_string($_POST['address']);
$city                         = escape_string($_POST['city']);
$country                      = escape_string($_POST['country']);
$registrationdate             = escape_string($_POST['registrationdate']);
$section                      = escape_string($_POST['section']);



$query = "UPDATE studgrade1 SET ";
$query .= "firstname                = '{$firstname}'                , ";
$query .= "middlename               = '{$middlename}'               , ";
$query .= "lastname                 = '{$lastname}'                 , ";
$query .= "dateofbirth              = '{$dateofbirth}'              , ";
$query .= "age                      = '{$age}'                      , ";
$query .= "gender                   = '{$gender}'                   , ";
$query .= "guardiancontactnumber    = '{$guardiancontactnumber}'    , ";
$query .= "address                  = '{$address}'                  , ";
$query .= "city                     = '{$city}'                     , ";
$query .= "country                  = '{$country}'                  , ";
$query .= "registrationdate         = '{$registrationdate}'         , ";
$query .= "section                  = '{$section}'                    ";
$query .= "WHERE idstudent=" . escape_string($_GET['id']);





$send_update_query = query($query);
confirm($send_update_query);
set_message("Student has been updated");
redirect("http://localhost/school/public/admin/MSGrd1.php");


        }


}

#Kingdergarten
function update_kindergarten() {


if(isset($_POST['update'])) {


$firstname                    = escape_string($_POST['firstname']);
$middlename                   = escape_string($_POST['middlename']);
$lastname                     = escape_string($_POST['lastname']);
$dateofbirth                  = escape_string($_POST['dateofbirth']);
$age                          = escape_string($_POST['age']);
$gender                       = escape_string($_POST['gender']);
$guardiancontactnumber        = escape_string($_POST['guardiancontactnumber']);
$address                      = escape_string($_POST['address']);
$city                         = escape_string($_POST['city']);
$country                      = escape_string($_POST['country']);
$registrationdate             = escape_string($_POST['registrationdate']);
$section                      = escape_string($_POST['section']);



$query = "UPDATE studkindergarten SET ";
$query .= "firstname                = '{$firstname}'                , ";
$query .= "middlename               = '{$middlename}'               , ";
$query .= "lastname                 = '{$lastname}'                 , ";
$query .= "dateofbirth              = '{$dateofbirth}'              , ";
$query .= "age                      = '{$age}'                      , ";
$query .= "gender                   = '{$gender}'                   , ";
$query .= "guardiancontactnumber    = '{$guardiancontactnumber}'    , ";
$query .= "address                  = '{$address}'                  , ";
$query .= "city                     = '{$city}'                     , ";
$query .= "country                  = '{$country}'                  , ";
$query .= "registrationdate         = '{$registrationdate}'         , ";
$query .= "section                  = '{$section}'                    ";
$query .= "WHERE idstudent=" . escape_string($_GET['id']);





$send_update_query = query($query);
confirm($send_update_query);
set_message("Student has been updated");
redirect("http://localhost/school/public/admin/MSkinder.php");


        }


}

#Nursery
function update_nursery() {


if(isset($_POST['update'])) {


$firstname                    = escape_string($_POST['firstname']);
$middlename                   = escape_string($_POST['middlename']);
$lastname                     = escape_string($_POST['lastname']);
$dateofbirth                  = escape_string($_POST['dateofbirth']);
$age                          = escape_string($_POST['age']);
$gender                       = escape_string($_POST['gender']);
$guardiancontactnumber        = escape_string($_POST['guardiancontactnumber']);
$address                      = escape_string($_POST['address']);
$city                         = escape_string($_POST['city']);
$country                      = escape_string($_POST['country']);
$registrationdate             = escape_string($_POST['registrationdate']);
$section                      = escape_string($_POST['section']);



$query = "UPDATE studnursery SET ";
$query .= "firstname                = '{$firstname}'                , ";
$query .= "middlename               = '{$middlename}'               , ";
$query .= "lastname                 = '{$lastname}'                 , ";
$query .= "dateofbirth              = '{$dateofbirth}'              , ";
$query .= "age                      = '{$age}'                      , ";
$query .= "gender                   = '{$gender}'                   , ";
$query .= "guardiancontactnumber    = '{$guardiancontactnumber}'    , ";
$query .= "address                  = '{$address}'                  , ";
$query .= "city                     = '{$city}'                     , ";
$query .= "country                  = '{$country}'                  , ";
$query .= "registrationdate         = '{$registrationdate}'         , ";
$query .= "section                  = '{$section}'                    ";
$query .= "WHERE idstudent=" . escape_string($_GET['id']);





$send_update_query = query($query);
confirm($send_update_query);
set_message("Student has been updated");
redirect("http://localhost/school/public/admin/MSnursery.php");


        }


}

#Preparatory
function update_preparatory() {


if(isset($_POST['update'])) {


$firstname                    = escape_string($_POST['firstname']);
$middlename                   = escape_string($_POST['middlename']);
$lastname                     = escape_string($_POST['lastname']);
$dateofbirth                  = escape_string($_POST['dateofbirth']);
$age                          = escape_string($_POST['age']);
$gender                       = escape_string($_POST['gender']);
$guardiancontactnumber        = escape_string($_POST['guardiancontactnumber']);
$address                      = escape_string($_POST['address']);
$city                         = escape_string($_POST['city']);
$country                      = escape_string($_POST['country']);
$registrationdate             = escape_string($_POST['registrationdate']);
$section                      = escape_string($_POST['section']);



$query = "UPDATE studpreparatory SET ";
$query .= "firstname                = '{$firstname}'                , ";
$query .= "middlename               = '{$middlename}'               , ";
$query .= "lastname                 = '{$lastname}'                 , ";
$query .= "dateofbirth              = '{$dateofbirth}'              , ";
$query .= "age                      = '{$age}'                      , ";
$query .= "gender                   = '{$gender}'                   , ";
$query .= "guardiancontactnumber    = '{$guardiancontactnumber}'    , ";
$query .= "address                  = '{$address}'                  , ";
$query .= "city                     = '{$city}'                     , ";
$query .= "country                  = '{$country}'                  , ";
$query .= "registrationdate         = '{$registrationdate}'         , ";
$query .= "section                  = '{$section}'                    ";
$query .= "WHERE idstudent=" . escape_string($_GET['id']);





$send_update_query = query($query);
confirm($send_update_query);
set_message("Student has been updated");
redirect("http://localhost/school/public/admin/MSprep.php");


        }


}






/***************************************OPTION ******************************************/
function teacheroption(){
	$query = query("SELECT * FROM teacher");
	confirm($query);

	while($row = fetch_array($query)){

$list_teacher = <<<DELIMITER

		<option value="{$row['lastname']}&nbsp;{$row['firstname']}">{$row['lastname']}&nbsp;{$row['firstname']}</option>

DELIMITER;

		echo $list_teacher;
	}
}


//Grade1
function sectiongrade1option(){
	$query = query("SELECT * FROM secgrade1");
	confirm($query);

	while($row = fetch_array($query)){

$list_section = <<<DELIMITER

		<option value="{$row['sectionname']}">{$row['sectionname']}</option>

DELIMITER;

		echo $list_section;
	}
}



/***************************************GRADE1 PREPARATORY*******************************************/
function secpreparatory(){
		$query = query("SELECT * FROM secpreparatory");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$secnursery = <<<DELIMITER
            <tr>
              <th scope="row">{$row['idsecpreparatory']}</th>
              <td>{$row['sectionname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                <a type="button" href="../../resources/templates/back/delete_secP.php?id={$row['idsecpreparatory']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $secnursery;

    		} 
}


/***************************************GRADE1 SECTION*******************************************/
function secgrade1(){
		$query = query("SELECT * FROM secgrade1");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$secnursery = <<<DELIMITER
            <tr>
              <th scope="row">{$row['idsecgrade1']}</th>
              <td>{$row['sectionname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                <a type="button" href="../../resources/templates/back/delete_secgrade1.php?id={$row['idsecgrade1']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $secnursery;

    		} 
}

/***************************************kingderagrten SECTION*******************************************/
function seckingderagrten(){
		$query = query("SELECT * FROM seckingderagrten");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$secnursery = <<<DELIMITER
            <tr>
              <th scope="row">{$row['idseckingderagrten']}</th>
              <td>{$row['sectionname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                <a type="button" href="../../resources/templates/back/delete_secK.php?id={$row['idseckingderagrten']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $secnursery;

    		} 
}

/***************************************NURSERY SECTION*******************************************/
function secnursery(){
		$query = query("SELECT * FROM secnursery");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$secnursery = <<<DELIMITER
            <tr>
              <th scope="row">{$row['idsecnursery']}</th>
              <td>{$row['sectionname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                 	<a type="button" href="../../resources/templates/back/delete_secN.php?id={$row['idsecnursery']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $secnursery;

    		} 
}

/***************************************NURSERY SECTION*******************************************/
function subjn(){
		$query = query("SELECT * FROM subjn");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$subjn = <<<DELIMITER
            <tr>
              <td>{$row['subjectname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                 	<a type="button" href="../../resources/templates/back/delete_subjn.php?id={$row['idsubjnursery']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $subjn;

    		} 
}

/***************************************KINDERGARTEN SECTION*******************************************/
function subjk(){
		$query = query("SELECT * FROM subjk");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$subjk = <<<DELIMITER
            <tr>
              <td>{$row['subjectname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                 	<a type="button" href="../../resources/templates/back/delete_subjk.php?id={$row['idsubjkindergarten']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $subjk;

    		} 
}

/***************************************PREPARATORY SECTION*******************************************/
function subjp(){
		$query = query("SELECT * FROM subjp");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$subjp = <<<DELIMITER
            <tr>
              <td>{$row['subjectname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                 	<a type="button" href="../../resources/templates/back/delete_subjp.php?id={$row['idsubjpreparatory']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $subjp;

    		} 
}

/***************************************GRADE 1 SECTION*******************************************/
function subjgrade1(){
		$query = query("SELECT * FROM subjgrade1");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$subjgrade1 = <<<DELIMITER
            <tr>
             
              <td>{$row['subjectname']}</td>
              <td>{$row['teachername']}</td>
              <td>
                 	<a type="button" class="btn btn-danger" href="../../resources/templates/back/delete_subjgrade1.php?id={$row['idsubjgrade1']}" onclick="return checkDelete()">Delete</a>
              </td>
            </tr>

DELIMITER;

echo $subjgrade1;

    		} 
}

/***************************************TEACHER LIST*******************************************/
function get_teacherlist(){
		$query = query("SELECT * FROM teacher");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$teacherlist = <<<DELIMITER

              <tr>
              <th scope="row">{$row['idteacher']}</th>
              <td>{$row['lastname']}</td>
              <td>{$row['middlename']}</td>
              <td>{$row['firstname']}</td>
              <td>{$row['contact']}</td>
              <td>{$row['username']}</td>
              <td>{$row['address']}</td>
              <td>{$row['city']}</td>
              <td>{$row['registerdate']}</td>
              <td>{$row['jobtype']}</td>
              <td>
                <a type="button" class="btn btn-danger" href="../../resources/templates/back/delete_teacher.php?id={$row['idteacher']}" onclick="return checkDelete()">Delete</a>
                 <a type="button" class="btn btn-info" href="../../resources/templates/back/teacherupdate.php?id={$row['idteacher']}" >Update</a>
              </td>
            </tr>

DELIMITER;
//href="../../resources/templates/back/delete_teacher.php?id={$row['idteacher']}"><span class="glyphicon glyphicon-remove"
echo $teacherlist;

    		} 

}


/***************************************PREPARATORY STUDENT*******************************************/
function msprep(){
		$query = query("SELECT * FROM studpreparatory");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$preparatoryoutput = <<<DELIMITER
<tr>
   <th scope="row">{$row['idstudent']}</th>
  <td>{$row['lastname']}</td>
  <td>{$row['middlename']}</td>
  <td>{$row['firstname']}</td>
  <td>{$row['dateofbirth']}</td>
  <td>{$row['age']}</td>
  <td>{$row['gender']}</td>
  <td>{$row['guardiancontactnumber']}</td>
  <td>{$row['section']}</td>
  <td>
    <a type="button" class="btn btn-danger" href="../../resources/templates/back/delete_studPrep.php?id={$row['idstudent']}" onclick="return checkDelete()">Delete</a>
    <a type="button" class="btn btn-info" href="../../resources/templates/back/updateStudMSprep.php?id={$row['idstudent']}">Update</a>
  </td>
</tr>

DELIMITER;

echo $preparatoryoutput;

    		} 

}


/***************************************NURSERY STUDENT*******************************************/
function msnursery(){
		$query = query("SELECT * FROM studnursery");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$nurseryoutput = <<<DELIMITER
<tr>
  <th scope="row">{$row['idstudent']}</th>
  <td>{$row['lastname']}</td>
  <td>{$row['middlename']}</td>
  <td>{$row['firstname']}</td>
  <td>{$row['dateofbirth']}</td>
  <td>{$row['age']}</td>
  <td>{$row['gender']}</td>
  <td>{$row['guardiancontactnumber']}</td>
  <td>{$row['section']}</td>  
  <td>
    <a type="button" class="btn btn-danger" href="../../resources/templates/back/delete_studNursery.php?id={$row['idstudent']}" onclick="return checkDelete()">Delete</a>
   <a type="button" class="btn btn-info" href="../../resources/templates/back/updateStudMSnursery.php?id={$row['idstudent']}">Update</a>   
  </td>
</tr>

DELIMITER;

echo $nurseryoutput;

    		} 

}




/***************************************KINDERGARTEN STUDENT*******************************************/
function mskinder(){
		$query = query("SELECT * FROM studkindergarten");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$kinderoutput = <<<DELIMITER
<tr>
  <th scope="row">{$row['idstudent']}</th>
  <td>{$row['lastname']}</td>
  <td>{$row['middlename']}</td>
  <td>{$row['firstname']}</td>
  <td>{$row['dateofbirth']}</td>
  <td>{$row['age']}</td>
  <td>{$row['gender']}</td>
  <td>{$row['guardiancontactnumber']}</td>
  <td>{$row['section']}</td>
  <td>
    <a type="button" class="btn btn-danger" href="../../resources/templates/back/delete_studKinder.php?id={$row['idstudent']}" onclick="return checkDelete()">Delete</a>
     <a type="button" class="btn btn-info" href="../../resources/templates/back/updateStudMSkinder.php?id={$row['idstudent']}">Update</a>
  </td>

</tr>

DELIMITER;

echo $kinderoutput;

    		} 

}


/***************************************GRADE1 STUDENT*******************************************/
function msgrd1(){
		$query = query("SELECT * FROM studgrade1");

		confirm($query);

    		while($row = fetch_array($query)) {
 

$msgrd1output = <<<DELIMITER
<tr>
  <th scope="row">{$row['idstudent']}</th>
  <td>{$row['lastname']}</td>
  <td>{$row['middlename']}</td>
  <td>{$row['firstname']}</td>
  <td>{$row['dateofbirth']}</td>
  <td>{$row['age']}</td>
  <td>{$row['gender']}</td>
  <td>{$row['guardiancontactnumber']}</td>
  <td>{$row['section']}</td>
  <td>
    <a type="button" class="btn btn-danger" href="../../resources/templates/back/delete_studGrd1.php?id={$row['idstudent']}" onclick="return checkDelete()">Delete</a>
    <a type="button" class="btn btn-info" href="../../resources/templates/back/updateStudMSGrd1.php?id={$row['idstudent']}">Update</a>
  </td>
</tr>

DELIMITER;

echo $msgrd1output;

    		} 

}


/*************************************************************************************************/
/****************************************END OF THE LINE******************************************/
/*************************************************************************************************/


/*************************************************************************************************/
/**********************************This will add to the database**********************************/
/*************************************************************************************************/


/*************************************NURSERY ADD BY SECTION***************************************/
function subjNadd(){
	if(isset($_POST['submit'])){

		$subjectname = $_POST['subjectname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO subjn(idsubjnursery, subjectname, teachername) VALUES (NULL, '$subjectname', '$teachername');");
		
		confirm($query);
       set_message("Subject Add");
    redirect("http://localhost/school/public/admin/subjn.php");
	}
}

/*************************************KINDERGARTEN ADD BY SECTION***************************************/
function subjKadd(){
	if(isset($_POST['submit'])){

		$subjectname = $_POST['subjectname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO subjk(idsubjkindergarten, subjectname, teachername) VALUES (NULL, '$subjectname', '$teachername');");
		
		confirm($query);
    set_message("Subject Add");
    redirect("http://localhost/school/public/admin/subjk.php");
	}
}

/*************************************PREPARATORY ADD BY SECTION***************************************/
function subjPadd(){
	if(isset($_POST['submit'])){

		$subjectname = $_POST['subjectname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO subjp(idsubjpreparatory, subjectname, teachername) VALUES (NULL, '$subjectname', '$teachername');");
		
		confirm($query);
     set_message("Subject Add");
    redirect("http://localhost/school/public/admin/subjp.php");
	}
}

/*************************************GRADE 1 ADD BY SECTION***************************************/
function subjgrade1add(){
	if(isset($_POST['submit'])){

		$subjectname = $_POST['subjectname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO subjgrade1(idsubjgrade1, subjectname, teachername) VALUES (NULL, '$subjectname', '$teachername');");
		
		confirm($query);
    redirect("http://localhost/school/public/admin/subjGrd1.php");
	}
}


/*************************************GRADE1 ADD BY SECTION***************************************/
function secgrade1add(){
	if(isset($_POST['submit'])){

		$sectionname = $_POST['sectionname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO secgrade1(idsecgrade1, sectionname, teachername) VALUES (NULL, '$sectionname', '$teachername');");
		
		confirm($query);
	}
}

/*************************************KINGDERAGRTEN ADD BY SECTION***************************************/
function secaddK(){
	if(isset($_POST['submit'])){

		$sectionname = $_POST['sectionname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO seckingderagrten(idseckingderagrten, sectionname, teachername) VALUES (NULL, '$sectionname', '$teachername');");
		
		confirm($query);
	}
}

/*************************************NURSERY ADD BY SECTION***************************************/
function secaddN(){
	if(isset($_POST['submit'])){

		$sectionname = $_POST['sectionname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO secnursery(idsecnursery, sectionname, teachername) VALUES (NULL, '$sectionname', '$teachername');");
		
		confirm($query);
	}
}

/*************************************NURSERY ADD BY SECTION***************************************/
function secaddP(){
	if(isset($_POST['submit'])){

		$sectionname = $_POST['sectionname'];
		$teachername = $_POST['teachername'];
		

		$query = query("INSERT INTO secpreparatory(idsecpreparatory, sectionname, teachername) VALUES (NULL, '$sectionname', '$teachername');");
		
		confirm($query);
	}
}

#REGISTER
function register(){
		if(isset($_POST['submit'])){

		$childname = $_POST['childname'];
		$gender = $_POST['gender'];
		$age = $_POST['age'];
		$birthdate = $_POST['birthdate'];
		$birthplace = $_POST['birthplace'];
		$religion = $_POST['religion'];
		$fathername = $_POST['fathername'];
		$mothername = $_POST['mothername'];
		$contactfather = $_POST['contactfather'];
		$contactmother = $_POST['contactmother'];
		$presentaddress = $_POST['presentaddress'];
		$application = $_POST['application'];
		$studentstatus = $_POST['studentstatus'];
		$iftransferee = $_POST['iftransferee'];
		$payment = $_POST['payment'];

		$query = query("INSERT INTO registerstep1(idregister, childname,  gender, age, birthdate, birthplace, religion, fathername, mothername,contactfather, contactmother, presentaddress, application, studentstatus, iftransferee, payment) VALUES (NULL, '$childname', '$gender', '$age','$birthdate', '$birthplace', '$religion', '$fathername', '$mothername','$contactfather', '$contactmother', '$presentaddress', '$application', '$studentstatus', '$iftransferee', '$payment');");
		
		confirm($query);
    set_message("Thank you for registration here your tracking number 1");
redirect("http://localhost/school/public/registerStep3.php");
	}

}

#adding teacher
function addteacher(){
	if(isset($_POST['submit'])){

		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
				$middlename = $_POST['middlename'];
		$dateofbirth = $_POST['dateofbirth'];
		$age = $_POST['age'];
		$contact = $_POST['contact'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$registerdate = $_POST['registerdate'];
		$jobtype = $_POST['jobtype'];

		$query = query("INSERT INTO teacher(idteacher, firstname,  lastname, middlename,dateofbirth, age, contact, username, password,address, city, registerdate, jobtype) VALUES (NULL, '$firstname', '$lastname', '$middlename','$dateofbirth', '$age', '$contact', '$username', '$password','$address', '$city', '$registerdate', '$jobtype');");
		
		confirm($query);


	}
}

#grade1 adding student
function grade1(){
	if(isset($_POST['submit'])){

		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$dateofbirth = $_POST['dateofbirth'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$guardiancontactnumber = $_POST['guardiancontactnumber'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$registrationdate = $_POST['registrationdate'];
		$section = $_POST['section'];

		$query = query("INSERT INTO studgrade1(idstudent, firstname, middlename, lastname, dateofbirth, age, gender, guardiancontactnumber, address, city, country, registrationdate, section) VALUES (NULL, '$firstname', '$middlename', '$lastname', '$dateofbirth', '$age', '$gender', '$guardiancontactnumber', '$address', '$city', '$country', '$registrationdate', '$section');");

		confirm($query);

		redirect("MSGrd1.php");

	}
}

#preparatory adding student
function preparatory(){
	if(isset($_POST['submit'])){

		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$dateofbirth = $_POST['dateofbirth'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$guardiancontactnumber = $_POST['guardiancontactnumber'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$registrationdate = $_POST['registrationdate'];
		$section = $_POST['section'];

		$query = query("INSERT INTO studpreparatory(idstudent, firstname, middlename, lastname, dateofbirth, age, gender, guardiancontactnumber, address, city, country, registrationdate, section) VALUES (NULL, '$firstname', '$middlename', '$lastname', '$dateofbirth', '$age', '$gender', '$guardiancontactnumber', '$address', '$city', '$country', '$registrationdate', '$section');");

		confirm($query);

		redirect("MSprep.php");

	}
}

#kindergarten adding student
function kindergarten(){
	if(isset($_POST['submit'])){

		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$dateofbirth = $_POST['dateofbirth'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$guardiancontactnumber = $_POST['guardiancontactnumber'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$registrationdate = $_POST['registrationdate'];
		$strand = $_POST['strand'];
		$section = $_POST['section'];

		$query = query("INSERT INTO studkindergarten(idstudent, firstname, middlename, lastname, dateofbirth, age, gender, guardiancontactnumber, address, city, country, registrationdate, section) VALUES (NULL, '$firstname', '$middlename', '$lastname', '$dateofbirth', '$age', '$gender', '$guardiancontactnumber', '$address', '$city', '$country', '$registrationdate', '$section');");

		confirm($query);

		redirect("MSKinder.php");

	}
}

#nursery adding student
function nursery(){
	if(isset($_POST['submit'])){

		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$dateofbirth = $_POST['dateofbirth'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$guardiancontactnumber = $_POST['guardiancontactnumber'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$registrationdate = $_POST['registrationdate'];
		$strand = $_POST['strand'];
		$section = $_POST['section'];

		$query = query("INSERT INTO studnursery(idstudent, firstname, middlename, lastname, dateofbirth, age, gender, guardiancontactnumber, address, city, country, registrationdate, section) VALUES (NULL, '$firstname', '$middlename', '$lastname', '$dateofbirth', '$age', '$gender', '$guardiancontactnumber', '$address', '$city', '$country', '$registrationdate', '$section');");

		confirm($query);

		redirect("MSnursery.php");

	}
}

/*************************************************************************************************/
/****************************************END OF THE LINE******************************************/
/*************************************************************************************************/

/*************************************************************************************************/
/*****************************************THE LOGIN CODE******************************************/
/*************************************************************************************************/



#login for admin 
function login_admin(){

	if(isset($_POST['submit'])){

		$username = escape_string($_POST['username']);
		$password = escape_string($_POST['password']);

		$query = query("SELECT * FROM admin WHERE username = '{$username}' AND password = '{$password}'");

		confirm($query);

		if (mysqli_num_rows($query) == 0) {
      
			redirect("admin_login.php");
		} else{
      $_SESSION['username'] = $username;
			redirect("http://localhost/school/public/admin/sec.php");
		}
	}
}


#login for teacher
function login_teacher(){
	if(isset($_POST['submit'])){

		$username = escape_string($_POST['username']);
		$password = escape_string($_POST['password']);

		$query = query("SELECT * FROM teacher WHERE username = '{$username}' AND password = '{$password}'");

		confirm($query);

		if (mysqli_num_rows($query) == 0) {
   
			redirect("teacher_login.php");
		} else{
       $_SESSION['username'] = $username;
			redirect("teacher/index.php");
		}
	}
}

/*************************************************************************************************/
/****************************************END OF THE LINE******************************************/
/*************************************************************************************************/

/*************************************************************************************************/
/****************************************END OF THE LINE******************************************/
/*************************************************************************************************/

/**************************BACK END FUNCTIONS*******************************************/

function registerview(){
    $query = query("SELECT * FROM registerstep1");

    confirm($query);

        while($row = fetch_array($query)) {
 

$registerview = <<<DELIMITER
            <tr>
              <th scope="row">{$row['idregister']}</th>
              <td>{$row['childname']}</td>
              <td>{$row['fathername']}</td>
              <td>{$row['mothername']}</td>
              <td>{$row['application']}</td>
              <td>{$row['studentstatus']}</td>
              <td>{$row['payment']}</td>
              <td>{$row['iftransferee']}</td>
            </tr>

DELIMITER;

echo $registerview;

        } 
}



?>